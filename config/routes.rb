#require 'api_constraints'
Rails.application.routes.draw do
  root :to => "users#index"
#  constraints subdomain: 'api' do
devise_for :users, :skip => [:registrations]
    scope module: 'api' do
      namespace :v1 do
        
        # get '/users/password/edit' => 'users#edit_password'
        resources :users, :only => [:index, :show, :create, :update, :destroy]
        resources :accounts, :only => [:new, :create, :show, :update]
        resources :sessions, :only => [:create, :show, :destroy]
        resources :clients #do

        resources :estimates do
          resources :takeoffs do
            post 'create_sheet' => 'sheets#new_sheet'
          end
          resources :proposals do
            post 'submit'
          end
        end
        post '/users/forget_password' => 'users#generate_password_email'
        post '/users/update_password' => 'users#update_password'
        post '/users/change_password_by_reset_token' => 'users/change_password_by_reset_token'
        post '/users/set_timezone' => 'users#set_timezone'
        post '/users/invite_user' => 'users#invite_user'
        post '/users/accept_invite' => 'users#accept_invite'
        post '/estimates/search'=> 'estimates#search_estimates'
        get '/custom_user_confirmation' => 'users#user_confirmation'
        match '/application.manifest' => Rails::Offline,:via=>[:get], :as=> :manifest
        #end
        resources :production_templates do
          put 'activate', on: :member
          put 'deactivate', on: :member
          collection do
            get :active
            get :inactive
          end
        end
        resources :production_rates
        #end
        resources :properties, :only => [:create, :update, :destroy] do
          resources :rooms, :only => [:create, :update, :destroy, :index]
          resources :sides, :only => [:create, :update, :destroy]
        end
        # resources :proposals do
        #   post 'submit'
        # end
        # a route for client to view their proposal
        resources :proposals
        get '/proposals/:id', to: 'proposals#guest_show'
        
        resources :global_rates
        resources :flex_rates
        resources :materials
        resources :measures
        resources :rate_categories
        resources :rates
        get 'oauth2calendarcallback' => 'estimates#authorize'
        get 'oauth2sheetscallback' => 'sheets#authorize'
        get 'oauth2calendarcallbackIonic' =>'estimates#ionic_authorize'

        
        #for test only
        resources :products
      end
    end
end
