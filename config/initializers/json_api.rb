ActiveSupport.on_load(:action_controller) do
  require 'active_model_serializers/register_jsonapi_renderer'
end

ActiveModelSerializers.config.adapter = :json
#ActiveModelSerializers.config.default_includes = '**'
#ActiveModelSerializers.config.key_transform = :unaltered
