Apartment.configure do |config|
  config.excluded_models = ['Account', 'AccountUser']
  config.tenant_names = -> { Account.pluck(:name) }
end
