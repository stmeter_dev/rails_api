require 'google/apis/sheets_v4'
require 'google/api_client/client_secrets'
require 'googleauth'
require 'googleauth/stores/file_token_store'
require 'fileutils'
require 'net/http'
require 'uri'

class Google::Sheets

  OOB_URI = ENV['OOB_URI']
  CREDENTIALS_PATH = File.join(Dir.home, '.credentials',
                             "calendarStore.yaml")
  API_KEY = ENV['GOOGLE_API_KEY']
  attr_writer :authorizer, :drive, :redirect_uri, :credentials

  def initialize
    puts "url is #{ENV['OOB_URI']}"
    FileUtils.mkdir_p(File.dirname(CREDENTIALS_PATH))
    client_id = Google::Auth::ClientId.from_file("#{Rails.root}/lib/google/client_secret.json")
    scope = ['https://www.googleapis.com/auth/spreadsheets']
    #token_store = Google::Auth::Stores::RedisTokenStore.new(redis: Redis.new)
    token_store = Google::Auth::Stores::FileTokenStore.new(file: CREDENTIALS_PATH)
    @authorizer = Google::Auth::UserAuthorizer.new(
    client_id, scope, token_store,'/v1/oauth2sheetscallback')
    user_id = 'default'
    @credentials = @authorizer.get_credentials(user_id)
    @service = Google::Apis::SheetsV4::SheetsService.new
    @service.authorization = @credentials
    @service.key = API_KEY
    puts "======> schema #{URI(OOB_URI).scheme}"
  end

  def authorization_url
    @authorizer.get_authorization_url(base_url: OOB_URI)
  end

  def get_credentials
    @credentials
  end

  def set_authorization
    @service.authorization = @credentials
  end

  def save_credentials(code)
    user_id = 'default'
    @credentials = @authorizer.get_and_store_credentials_from_code(
      user_id: user_id, code: code, base_url: OOB_URI)
    @service.authorization = @credentials
  end

  def create_new_sheet(takeoff, client, property,email)

    properties = Google::Apis::SheetsV4::SpreadsheetProperties.new(
      title: "#{client.full_name} Takeoff -- #{Time.now.strftime("%d/%m/%Y %H:%M")}"
    )
    @request = Google::Apis::SheetsV4::Spreadsheet.new
    @request.properties = properties
    @spread_sheet = @service.create_spreadsheet(@request)
    @sheet_url = @spread_sheet.spreadsheet_url

    spreadsheet_id = JSON.parse(@spread_sheet.to_json)['spreadsheetId']
    #Construct room rows
    room_header = ["name", "Quantity", "Unit", "Labor(H)", "Material(G)", "Cost"]
    sheet_values = [
         [],
         [],
         [],
         ["Painting Takeoff Report", "", "", "Estimator", "#{email}", ""],
         ["", "", "", "Date", takeoff[:start].strftime("%d/%m/%Y %H:%M"), ""],
         [],
         ["Customer Information"],
         [],
         ["Name", "#{client.full_name}"],
         ["Email", "#{client.email}"],
         ["Phone", "#{client.phone_number}"],
         ["Address", "#{property.full_address}"],
         [],
         ["Interior Takeoff Details"],
         [""], ### Insert Room Rows Here
         ["Job Summary"],
         [],
         ["Total Interior Labor Hours", "", "", "", "", takeoff[:work_hours]],
         ["Total Interior Labor Cost", "", "", "", "", "$#{takeoff[:job_labor_cost]}"],
         ["Total Interior Material (Gallons)", "", "", "", "", takeoff[:job_materials]],
         ["Total Interior Material Cost", "", "", "", "", "$#{takeoff[:job_material_cost]}"],
         ["Total Cost", "", "", "", "", "$#{takeoff[:job_total_cost]}"],
         ["Sales Tax", "", "", "", "", "$#{takeoff[:job_tax]}"],
         ["Profit", "", "", "", "", "$#{takeoff[:job_profit]}"],
         [],
         ["Total Job Price", "", "", "", "", "$#{takeoff[:job_price]}"],
         [],
         []
       ]

    ## construct the format hash whihc has all the indeces of start rows
    format_indices = {section: [6, 13], room_h: [], room_f: [], room_b: [], footer: {}}
    index = 15
    takeoff[:rooms].each_with_index do |room, i|
      format_indices[:room_h].push(index)
      format_indices[:room_b].push({start: index + 1, end: index + 1 + room[:room_features_attributes].try(:size)})
      format_indices[:room_f].push(index + room[:room_features_attributes].try(:size).try(:+, 1))
      room_header[0] = room[:name]
      sheet_values.insert(index, room_header.slice(0, room_header.count))
      index += 1
      room[:room_features_attributes].each do |item|
        item_row = []
        item_row.push(item[:name], item[:quantity], "", item[:hours], item[:material_quantity], item[:labor_cost] + item[:material_cost])
        sheet_values.insert(index, item_row)
        index += 1
      end
      subtotal = []
      subtotal.push(room[:name] + " Subtotal", "", "", room[:hours], room[:material_quantity], room[:total_cost])
      sheet_values.insert(index, subtotal)
      index += 1
      sheet_values.insert(index, [])
      index += 1
    end

    format_indices[:section].push(format_indices[:room_f].last.try(:+, 2))
    format_indices[:footer][:start] = format_indices[:section].last.try(:+, 2)
    format_indices[:footer][:end] = format_indices[:footer][:start].try(:+, 7)


    #rows_count initial value is 45
    range = "Sheet1!A1:F#{sheet_values.count}"
    value_range_object = {
      major_dimension: "ROWS",
      values: sheet_values
    }

    @service.update_spreadsheet_value(
      spreadsheet_id, range, value_range_object,
      value_input_option: 'USER_ENTERED')


    #formatting the spreadsheet

    #construct the requests array for sheet formatting
    requests = [
        {
          "updateSheetProperties": {
            "properties": {
              "sheetId": 0,
              "title": "Interior Takeoff",
              "gridProperties": {
                "rowCount": sheet_values.size,
                "columnCount": 6,
                "hideGridlines": true
              }
            },
            "fields": "*"
          }
        },
        {
          "repeatCell": {
            "range": {
              "startRowIndex": 0,
              "endRowIndex": sheet_values.count - 1
            },
            "cell": {
              "userEnteredFormat": {
                "textFormat": {
                  "foregroundColor": {
                    "red": 226,
                    "green": 189,
                    "blue": 179,
                    "alpha": 1
                  },
                  "fontFamily": "Roboto",
                  "fontSize": 10
                }
              }
            },
            "fields": "userEnteredFormat(textFormat)"
          }
        },
        {
          "updateDimensionProperties": {
            "range": {
              "dimension": "ROWS",
              "startIndex": 0,
              "endIndex": 1
            },
            "properties": {
              "pixelSize": 8
            },
            "fields": "pixelSize"
          }
        },
        {
          "mergeCells": {
            "range": {
              "startRowIndex":2,
              "endRowIndex": 5,
              "endColumnIndex": 3
            },
            "mergeType": "MERGE_ALL"
          }
        },
        {
          "repeatCell": {
            "range": {
              "startRowIndex": 2,
              "endRowIndex": 5,
              "startColumnIndex": 0,
              "endColumnIndex": 3
            },
            "cell": {
              "userEnteredFormat": {
                "horizontalAlignment": "LEFT",
                "verticalAlignment": "MIDDLE",
                "textFormat": {
                  "fontSize": 20,
                  "bold": true
                }
              }
            },
            "fields": "userEnteredFormat(horizontalAlignment, verticalAlignment, textFormat.fontSize, textFormat.bold)"
          }
        },
        {
          "repeatCell": {
            "range": {
              "startRowIndex": 0,
              "endRowIndex": 1
            },
            "cell": {
              "userEnteredFormat": {
                "backgroundColor": {
                  "red": 226,
                  "green": 189,
                  "blue": 179,
                  "alpha": 1
                }
              }
            },
            "fields": "userEnteredFormat(backgroundColor)"
          }
        },
        {
          "repeatCell": {
            "range": {
              "startRowIndex": sheet_values.count - 3,
              "endRowIndex": sheet_values.count - 2
            },
            "cell": {
              "userEnteredFormat": {
                "textFormat": {
                  "fontSize": 13,
                  "bold": true
                }
              }
            },
            "fields": "userEnteredFormat(textFormat.fontSize, textFormat.bold)"
          }
        },
        {
          "mergeCells": {
            "range": {
              "startRowIndex":  sheet_values.count - 3,
              "endRowIndex": sheet_values.count - 2,
              "endColumnIndex": 5
            },
            "mergeType": "MERGE_ALL"
          }
        },
        {
          "repeatCell": {
            "range": {
              "startRowIndex": sheet_values.count - 3,
              "endRowIndex": sheet_values.count - 2
            },
            "cell": {
              "userEnteredFormat": {
                "textFormat": {
                  "foregroundColor": {
                    "red": 1,
                    "green": 1,
                    "blue": 1,
                    "alpha": 1
                  }
                },
                "backgroundColor": {
                  "red": 226,
                  "green": 189,
                  "blue": 179,
                  "alpha": 1
                }
              }
            },
            "fields": "userEnteredFormat(textFormat.foregroundColor, backgroundColor)"
          }
        }
    ]

    format_indices.each do |key, value|
      case key.to_s
      when "section"
        value.compact.each do |row_index|
          requests.push(
            {
              "mergeCells": {
                "range": {
                  "startRowIndex": row_index,
                  "endRowIndex": row_index + 1,
                  "startColumnIndex": 0,
                  "endColumnIndex": 6
                },
                "mergeType": "MERGE_ALL"
              }
            },
            {
              "repeatCell": {
                "range": {
                  "startRowIndex": row_index,
                  "endRowIndex": row_index + 1
                },
                "cell": {
                  "userEnteredFormat": {
                    "backgroundColor": {
                      "red": 226,
                      "green": 189,
                      "blue": 179,
                      "alpha": 1
                    },
                    "textFormat": {
                      "foregroundColor": {
                        "red": 1,
                        "green": 1,
                        "blue": 1,
                        "alpha": 1
                      },
                      "fontSize": 12,
                      "bold": true
                    }
                  }
                },
                "fields": "userEnteredFormat(backgroundColor, textFormat.fontSize,
                textFormat.foregroundColor, textFormat.bold)"
              }
            }
            )
        end
      when "room_h"
        value.each do |row_index|
          requests.push(
            {
              "repeatCell": {
                "range": {
                  "startRowIndex": row_index,
                  "endRowIndex": row_index + 1,
                  "startColumnIndex": 0,
                  "endColumnIndex": 6
                },
                "cell": {
                  "userEnteredFormat": {
                    "verticalAlignment": "MIDDLE",
                    "textFormat": {
                      "foregroundColor": {
                        "red": 226,
                        "green": 189,
                        "blue": 179,
                        "alpha": 1
                      },
                      "fontSize": 12,
                      "bold": true
                    }
                  }
                },
                "fields": "userEnteredFormat(verticalAlignment,
                          textFormat.foregroundColor, textFormat.fontSize, textFormat.bold)"
              }
            },
            {
              "repeatCell": {
                "range": {
                  "startRowIndex": row_index,
                  "endRowIndex": row_index + 1,
                  "startColumnIndex": 1,
                  "endColumnIndex": 6
                },
                "cell": {
                  "userEnteredFormat": {
                    "horizontalAlignment": "RIGHT"
                  }
                },
                "fields": "userEnteredFormat(horizontalAlignment)"
              }
            },
            {
              "repeatCell": {
                "range": {
                  "startRowIndex": row_index,
                  "endRowIndex": row_index + 1,
                  "startColumnIndex": 0,
                  "endColumnIndex": 1
                },
                "cell": {
                  "userEnteredFormat": {
                    "horizontalAlignment": "LEFT"
                  }
                },
                "fields": "userEnteredFormat(horizontalAlignment)"
              }
            }
            )
        end

        when "room_f"
          value.each do |row_index|
            requests.push(
              {
              "repeatCell": {
                "range": {
                  "startRowIndex": row_index,
                  "endRowIndex": row_index + 1
                },
                "cell": {
                  "userEnteredFormat": {
                    "textFormat": {
                      "bold": true
                    },
                    "backgroundColor": {
                      "red": 67,
                      "green": 67,
                      "blue": 67,
                      "alpha": 1
                    }
                  }
                },
                "fields": "userEnteredFormat(textFormat.bold, backgroundColor)"
              }
            },
            {
              "mergeCells": {
                "range": {
                  "startRowIndex": row_index,
                  "endRowIndex": row_index + 1,
                  "startColumnIndex": 0,
                  "endColumnIndex": 3
                },
                "mergeType": "MERGE_ALL"
              }
            },
            {
              "repeatCell": {
                "range": {
                  "startRowIndex": row_index,
                  "endRowIndex": row_index + 1,
                  "startColumnIndex": 3,
                  "endColumnIndex": 6
                },
                "cell": {
                  "userEnteredFormat": {
                    "horizontalAlignment": "RIGHT"
                  }
                },
                "fields": "userEnteredFormat(horizontalAlignment)"
              }
            }
            )
          end

        when "room_b"
          value.each do |hash|
            requests.push(
              {
                "addBanding": {
                  "bandedRange": {
                    "range": {
                      "startRowIndex": hash[:start],
                      "endRowIndex": hash[:end],
                      "startColumnIndex": 0,
                      "endColumnIndex": 6
                    },
                    "rowProperties": {
                      "firstBandColor": {
                        "red": 13,
                        "green": 13,
                        "blue": 13
                      },
                      "secondBandColor": {
                        "red": 1,
                        "green": 1,
                        "blue": 1
                      }
                    }
                  }
                }
              },
              {
                "repeatCell": {
                  "range": {
                    "startRowIndex": hash[:start],
                    "endRowIndex": hash[:end],
                    "startColumnIndex": 1,
                    "endColumnIndex": 6
                  },
                  "cell": {
                    "userEnteredFormat": {
                      "horizontalAlignment": "RIGHT"
                    }
                  },
                  "fields": "userEnteredFormat(horizontalAlignment)"
                }
              },
              {
                "repeatCell": {
                  "range": {
                    "startRowIndex": hash[:start],
                    "endRowIndex": hash[:end],
                    "startColumnIndex": 0,
                    "endColumnIndex": 1
                  },
                  "cell": {
                    "userEnteredFormat": {
                      "horizontalAlignment": "LEFT"
                    }
                  },
                  "fields": "userEnteredFormat(horizontalAlignment)"
                }
              },
              {
                "repeatCell": {
                  "range": {
                    "startRowIndex": hash[:start],
                    "endRowIndex": hash[:end] + 1,
                    "startColumnIndex": 5,
                    "endColumnIndex": 6
                  },
                  "cell": {
                    "userEnteredFormat": {
                      "numberFormat": {
                        "type": "CURRENCY",
                        "pattern": "$#,##0.00"
                      }
                    }
                  },
                  "fields": "userEnteredFormat.numberFormat"
                }
              }
            )
          end
        when "footer"
          requests.push(
             {
                "addBanding": {
                  "bandedRange": {
                    "range": {
                      "startRowIndex": value[:start],
                      "endRowIndex": value[:end],
                      "startColumnIndex": 0,
                      "endColumnIndex": 6
                    },
                    "rowProperties": {
                      "firstBandColor": {
                        "red": 13,
                        "green": 13,
                        "blue": 13
                      },
                      "secondBandColor": {
                        "red": 1,
                        "green": 1,
                        "blue": 1
                      }
                    }
                  }
                }
              },
              {
                "repeatCell": {
                  "range": {
                    "startRowIndex": value[:start],
                    "endRowIndex": value[:end]
                  },
                  "cell": {
                    "userEnteredFormat": {
                      "textFormat": {
                        "fontSize": 11,
                        "bold": true
                      }
                    }
                  },
                  "fields": "userEnteredFormat(textFormat.fontSize, textFormat.bold)"
                }
              }
          )
      end
    end

    ############# submit the request to googleapis ###############
    url = 'https://sheets.googleapis.com/v4/spreadsheets/'
    uri = URI.parse(url + spreadsheet_id + ':batchUpdate')
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    req = Net::HTTP::Post.new(uri.request_uri)
    req['Content-Type'] = 'application/json'
    req['Authorization'] = 'Bearer ' + @credentials.access_token
    req.body = {"requests": requests}.to_json
    resp = http.request(req)

    return @sheet_url;

  end

  def get_sheet
    @sheet
  end

  private

  def header_index(sheet_values, header)
    sheet_values.each_with_index do |row, i|
      row.each_with_index do |value, j|
        return i if value == header
      end
    end
  end

end
