require 'google/apis/calendar_v3'
require 'google/api_client/client_secrets'
require 'googleauth'
require 'googleauth/stores/file_token_store'
require 'net/http'
require 'uri'

class Google::Calendar

  OOB_URI = ENV['OOB_URI']
  CREDENTIALS_PATH = File.join(Dir.home, '.credentials', "calendarStore.yaml")
  puts "credentials path: #{CREDENTIALS_PATH}"
  API_KEY = ENV['GOOGLE_API_KEY']

  attr_writer :authorizer, :service, :redirect_uri, :credentials

  def initialize
    FileUtils.mkdir_p(File.dirname(CREDENTIALS_PATH))
    @client_id = Google::Auth::ClientId.from_file("#{Rails.root}/lib/google/client_secret.json")
    @scope = ['https://www.googleapis.com/auth/calendar', 'https://www.googleapis.com/auth/spreadsheets']
    #token_store = Google::Auth::Stores::RedisTokenStore.new(redis: Redis.new)
    @token_store = Google::Auth::Stores::FileTokenStore.new(file: CREDENTIALS_PATH)
    @authorizer = Google::Auth::UserAuthorizer.new(
        @client_id,
        @scope,
        @token_store,'/v1/oauth2calendarcallback'
    )

    user_id = 'default'
    @credentials = @authorizer.get_credentials(user_id)
    @service = Google::Apis::CalendarV3::CalendarService.new
    @service.authorization = @credentials
    @service.key = API_KEY
  end

  # This is a hack
  # this is not final please refactor the entire code
  # Not a right way to write a service
  def init_authorizer(device_type)
    call_backurl = device_type == 'ionic' ? 'oauth2calendarcallback': 'oauth2calendarcallback'
    @authorizer = Google::Auth::UserAuthorizer.new(
        @client_id,
        @scope,
        @token_store,
        "/v1/#{call_backurl}")
  end


  def authorization_url
    @authorizer.get_authorization_url(base_url: OOB_URI)
  end

  def get_credentials
    @credentials
  end

  def save_credentials(code)
    user_id = 'default'
    @credentials = @authorizer.get_and_store_credentials_from_code(
      user_id: user_id, code: code, base_url: OOB_URI)
  end

  def list_events(options = {})
    calendar_id = 'primary'
    @response = @service.list_events(calendar_id,
                       max_results: 10,
                       single_events: true,
                       order_by: 'startTime',
                       time_min: Time.now.iso8601)
    @response
  end

  def new_event(event)
    calendarEvent = Google::Apis::CalendarV3::Event.new(event)
    @service.authorization = @credentials
    options = {
     'send_notifications': true
    }
    @result = @service.insert_event('primary', calendarEvent,options)
    return @result.html_link
  end
end
