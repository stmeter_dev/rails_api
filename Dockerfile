FROM ruby:2.2.4
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev postgresql-client-9.4

RUN mkdir /stmeter_api
WORKDIR /stmeter_api

RUN echo 'gem: --no-rdoc --no-ri' >> "$RAILS_ROOT/.gemrc"
ADD Gemfile Gemfile
ADD Gemfile.lock Gemfile.lock
ENV RAILS_ENV production

RUN gem install bundler
RUN bundle install
ADD . /stmeter_api

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
