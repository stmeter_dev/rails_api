require 'test_helper'

class ProposalMailerTest < ActionMailer::TestCase
  test "submit_proposal" do
    mail = ProposalMailer.submit_proposal
    assert_equal "Submit proposal", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
