# Preview all emails at http://localhost:3000/rails/mailers/proposal_mailer
class ProposalMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/proposal_mailer/submit_proposal
  def submit_proposal
    proposal = Proposal.first
    ProposalMailer.submit_proposal(proposal)
  end

end
