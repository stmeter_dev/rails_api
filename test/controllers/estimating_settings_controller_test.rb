require 'test_helper'

class EstimatingSettingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @estimating_setting = estimating_settings(:one)
  end

  test "should get index" do
    get estimating_settings_url, as: :json
    assert_response :success
  end

  test "should create estimating_setting" do
    assert_difference('EstimatingSetting.count') do
      post estimating_settings_url, params: { estimating_setting: { labor: @estimating_setting.labor, profit: @estimating_setting.profit, tax: @estimating_setting.tax } }, as: :json
    end

    assert_response 201
  end

  test "should show estimating_setting" do
    get estimating_setting_url(@estimating_setting), as: :json
    assert_response :success
  end

  test "should update estimating_setting" do
    patch estimating_setting_url(@estimating_setting), params: { estimating_setting: { labor: @estimating_setting.labor, profit: @estimating_setting.profit, tax: @estimating_setting.tax } }, as: :json
    assert_response 200
  end

  test "should destroy estimating_setting" do
    assert_difference('EstimatingSetting.count', -1) do
      delete estimating_setting_url(@estimating_setting), as: :json
    end

    assert_response 204
  end
end
