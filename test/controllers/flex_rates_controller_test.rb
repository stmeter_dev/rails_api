require 'test_helper'

class FlexRatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @flex_rate = flex_rates(:one)
  end

  test "should get index" do
    get flex_rates_url, as: :json
    assert_response :success
  end

  test "should create flex_rate" do
    assert_difference('FlexRate.count') do
      post flex_rates_url, params: { flex_rate: { description: @flex_rate.description, name: @flex_rate.name, rate: @flex_rate.rate } }, as: :json
    end

    assert_response 201
  end

  test "should show flex_rate" do
    get flex_rate_url(@flex_rate), as: :json
    assert_response :success
  end

  test "should update flex_rate" do
    patch flex_rate_url(@flex_rate), params: { flex_rate: { description: @flex_rate.description, name: @flex_rate.name, rate: @flex_rate.rate } }, as: :json
    assert_response 200
  end

  test "should destroy flex_rate" do
    assert_difference('FlexRate.count', -1) do
      delete flex_rate_url(@flex_rate), as: :json
    end

    assert_response 204
  end
end
