require 'test_helper'

class RateCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @rate_category = rate_categories(:one)
  end

  test "should get index" do
    get rate_categories_url, as: :json
    assert_response :success
  end

  test "should create rate_category" do
    assert_difference('RateCategory.count') do
      post rate_categories_url, params: { rate_category: { inout: @rate_category.inout, mode: @rate_category.mode, name: @rate_category.name, unit: @rate_category.unit } }, as: :json
    end

    assert_response 201
  end

  test "should show rate_category" do
    get rate_category_url(@rate_category), as: :json
    assert_response :success
  end

  test "should update rate_category" do
    patch rate_category_url(@rate_category), params: { rate_category: { inout: @rate_category.inout, mode: @rate_category.mode, name: @rate_category.name, unit: @rate_category.unit } }, as: :json
    assert_response 200
  end

  test "should destroy rate_category" do
    assert_difference('RateCategory.count', -1) do
      delete rate_category_url(@rate_category), as: :json
    end

    assert_response 204
  end
end
