require 'test_helper'

class TakeoffsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @takeoff = takeoffs(:one)
  end

  test "should get index" do
    get takeoffs_url, as: :json
    assert_response :success
  end

  test "should create takeoff" do
    assert_difference('Takeoff.count') do
      post takeoffs_url, params: { takeoff: { end: @takeoff.end, job_labor_cost: @takeoff.job_labor_cost, job_material_cost: @takeoff.job_material_cost, job_price: @takeoff.job_price, job_profit: @takeoff.job_profit, job_tax: @takeoff.job_tax, job_total_cost: @takeoff.job_total_cost, saved: @takeoff.saved, start: @takeoff.start, status: @takeoff.status, work_hours: @takeoff.work_hours } }, as: :json
    end

    assert_response 201
  end

  test "should show takeoff" do
    get takeoff_url(@takeoff), as: :json
    assert_response :success
  end

  test "should update takeoff" do
    patch takeoff_url(@takeoff), params: { takeoff: { end: @takeoff.end, job_labor_cost: @takeoff.job_labor_cost, job_material_cost: @takeoff.job_material_cost, job_price: @takeoff.job_price, job_profit: @takeoff.job_profit, job_tax: @takeoff.job_tax, job_total_cost: @takeoff.job_total_cost, saved: @takeoff.saved, start: @takeoff.start, status: @takeoff.status, work_hours: @takeoff.work_hours } }, as: :json
    assert_response 200
  end

  test "should destroy takeoff" do
    assert_difference('Takeoff.count', -1) do
      delete takeoff_url(@takeoff), as: :json
    end

    assert_response 204
  end
end
