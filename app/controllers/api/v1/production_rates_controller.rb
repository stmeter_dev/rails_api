class Api::V1::ProductionRatesController < ApplicationController

  before_action :authenticate_with_token!
  before_action :find_prod_rate, only: [:update, :activate, :deactivate, :destroy]

  def show
    render json: ProductionRate.find(params[:id])
  end

  def index
    render json: ProductionRate.all
  end

  def create
    @production_rate = ProductionRate.new(prod_rate_params)
    if @production_rate.save
      render json: @production_rate, status: 201
    else
      render json: { errors: @production_rate.errors }, status: 422
    end
  end

  def update
    if @production_rate.update(prod_rate_params)
      render json: @production_rate, status: 201
    else
      render json: { errors: @production_rate.errors }, status: 422
    end
  end

  def destroy
    @production_rate.destroy
    head 204
  end

  private

  def prod_rate_params
    params.require(:production_rate).permit(:name, :rate, :unit, :category)
  end

  def find_prod_rate
    @production_rate = ProductionRate.find(params[:id])
  end

end
