class Api::V1::AccountsController < ApplicationController
  before_action :authenticate_with_token!, only: [:update]
  def new
    @account =  Account.new
    @account.build_owner
  end

  def create
    @account = Account.new(account_params)
    if @account.valid?
      # save the record in the UserAccount table to public schema
      Apartment::Tenant.switch!('public')
      # ActiveRecord::Base.connection.execute(
      #   "INSERT INTO account_users (email, account)
      #    VALUES ('#{account_params[:owner_attributes][:email]}', '#{account_params[:name]}');")

      @account_user = AccountUser.new(email: account_params[:owner_attributes][:email],
        account: account_params[:name])
      if @account_user.valid?
        @account_user.save
        Apartment::Tenant.create(@account.name)
        # Create the Tenant
        Apartment::Tenant.switch!(@account.name)
        #save the Account in the corresponding Tenant
        @account.save
        # Service to seed tenant with the data.
        seed_service = AccountsModule::SeedNewAccount.new
        seed_service.call
        render json: @account, status: 201
      else
        render json: { errors: @account_user.errors }, status: 422
      end
    else
      render json: { errors: @account.errors }, status: 422
    end
  end

  def update
    @account = Account.find(params[:id])
    if @account.update(account_params)
      render json: @account, status: 200
    else
      render json: { errors: @account.errors }, status: 422
    end

  end

  def show
    @account = Account.find(params[:id])
    render json: @account , status: 200
  end

  private

  def account_params
    params.require(:account).permit(:name, :addr_line1, :addr_line2, :zipcode,
    :city, :state, :logo, :phone_number, :cell_number, :fax, :time_zone,
    owner_attributes: [:name, :email, :password, :password_confirmation, :time_zone])
  end

end
