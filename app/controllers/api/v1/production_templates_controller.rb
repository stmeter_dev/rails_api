class Api::V1::ProductionTemplatesController < ApplicationController

  #before_action :authenticate_with_token!
  before_action :find_prod_template, only: [:update, :activate, :deactivate, :destroy]

  def show
    render json: ProductionTemplate.includes(:production_rates).find(params[:id])
  end

  def index
    render json: ProductionTemplate.includes(:production_rates).order('updated_at DESC')
  end

  def active
    render json: ProductionTemplate.active.includes(:production_rates)
  end

  def activate
    @active_template = ProductionTemplate.active
    @active_template.update(active: false)
    @production_template.update(active: true)
    head 204
  end

  def deactivate
    @production_template.update(active: false)
    head 204
  end

  def create
    production_template = ProductionTemplate.new(prod_template_params)
    if production_template.save
      render json: production_template, status: 201
    else
      render json: { errors: production_template.errors }, status: 422
    end
  end

  def update
    if @production_template.update(prod_template_params)
      render json: @production_template, status: 201
    else
      render json: { errors: @production_template.errors }, status: 422
    end
  end

  def destroy
    @production_template.destroy
    head 204
  end

  private

  def prod_template_params
    params.require(:production_template).permit(:id, :name, :active,
     production_rates_attributes: [:id, :name,
     :unit, :rate, :category, :production_template_id, :_destroy])
  end

  def find_prod_template
    @production_template = ProductionTemplate.find(params[:id])
  end

end
