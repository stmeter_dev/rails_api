class Api::V1::FlexRatesController < ApplicationController
  before_action :authenticate_with_token!
  before_action :set_flex_rate, only: [:show, :update, :destroy]

  # GET /flex_rates
  def index
    @flex_rates = FlexRate.all

    render json: @flex_rates
  end

  # GET /flex_rates/1
  def show
    render json: @flex_rate
  end

  # POST /flex_rates
  def create
    @flex_rate = FlexRate.new(flex_rate_params)

    if @flex_rate.save
      render json: @flex_rate, status: :created
    else
      #render json: @flex_rate.errors, status: :unprocessable_entity
      render json: { errors: @flex_rate.errors }, status: 422
    end
  end

  # PATCH/PUT /flex_rates/1
  def update
    if @flex_rate.update(flex_rate_params)
      render json: @flex_rate
    else
      render json: @flex_rate.errors, status: :unprocessable_entity
    end
  end

  # DELETE /flex_rates/1
  def destroy
    @flex_rate.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_flex_rate
      @flex_rate = FlexRate.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def flex_rate_params
      params.require(:flex_rate).permit(:name, :description, :rate, :unit)
    end
end
