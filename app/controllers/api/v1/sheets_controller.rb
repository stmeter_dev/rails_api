class Api::V1::SheetsController < ApplicationController
  before_action :authenticate, except: [:set_auth_token, :authorize]
  before_action :set_estimate, except: [:set_auth_token, :authorize]
  before_action :set_takeoff, except: [:set_auth_token, :authorize]

  def new_sheet
    @current_user ||= User.find_by(auth_token: request.headers['Authorization']);
    email = @current_user.email

    localTakeoff = create_takeoff(@takeoff)

    client = @takeoff.estimate.property.client

    property = @takeoff.estimate.property

    new_sheet_url = $sheets.create_new_sheet(localTakeoff, client, property,email);

    @takeoff.update(sheet_url: new_sheet_url)

    render json: { status: :ok, sheet: new_sheet_url }
  end

  def set_auth_token
    if request['code'] == nil
      redirect_to($sheets.authorization_url)
    else
      $sheets.save_credentials(request['code'])
      new_sheet
    end
  end

  def authorize
    if $sheets.get_credentials
      render json: { authorized: 'OK' }, status: 200
    elsif request['code'] == nil
      #response.headers['Access-Control-Allow-Origin'] = '*'
      #redirect_to $calendar.authorization_url
      render json: { authorized: 'NOK', redirect_to: $sheets.authorization_url }, status: 200
    else
      $sheets.save_credentials(request['code'])
      redirect_to "#{ENV['ANGULAR_URL']}/sales?code=#{request['code']}"
    end
  end

  def create_takeoff(takeoff)
    {
      "start": takeoff.start,
      "work_hours": takeoff.work_hours,
      "job_labor_cost": takeoff.job_labor_cost,
      "job_materials": takeoff.job_materials,
      "job_material_cost": takeoff.job_material_cost,
      "job_total_cost": takeoff.job_total_cost,
      "job_tax": takeoff.job_tax,
      "job_profit": takeoff.job_profit,
      "job_price": takeoff.job_price,
      "rooms": setRoomData(takeoff.rooms)
    }
  end

  private

  def set_estimate
    @estimate = Estimate.find(params[:estimate_id])
  end

  def set_takeoff
    @takeoff = Takeoff.find(takeoff_params[:id])
  end

    # Only allow a trusted parameter "white list" through.
  def takeoff_params
    params.require(:takeoff).permit(:id, :start, :end, :status, :saved, :work_hours,
              :job_labor_cost, :job_material_cost, :job_total_cost, :job_profit,
              :job_tax, :job_price, :rooms, :sides, :total_cost)
  end

  def authenticate
    unless $sheets.get_credentials
      set_auth_token
    end
  end

  # =============== Data Creation ===============


  def setRoomData(rooms)

    roomsData = [];

    rooms.each do | room |
      room_obj = {
        name: room.name,
        material_quantity: room.material_quantity,
        labor_cost: room.labor_cost,
        material_cost: room.material_cost,
        total_cost: room.total_cost,
        hours: room.hours,
        room_features_attributes: getRoomsFeatures(room.room_features)
      }
      roomsData.push(room_obj)
    end
    return roomsData

  end

  def getRoomsFeatures(room_features)
    room_features_data = [];

    room_features.each do |feature|
      feature_obj = {
        name: feature.name,
        material_cost: feature.material_cost,
        quantity: feature.quantity,
        hours: feature.hours,
        labor_cost: feature.labor_cost,
        material_quantity: feature.material_quantity
      }
      room_features_data.push(feature_obj)
    end
    return room_features_data
  end
end
