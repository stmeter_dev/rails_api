class Api::V1::ProposalsController < ApplicationController

  before_action :authenticate_with_token!, except: [:index, :show, :guest_show]

  def show
    #working for displaying proposal to Front End app
    # render json: Proposal.includes(:estimate,
    #             interior_scope: [{room_scopes: [:room_items]}],
    #             exterior_scope: [{side_scopes: [:side_items]}]
    # ).find(params[:id])

    #show action with pdf from rails
    @proposal = Proposal.find(params[:id])
    respond_to do |format|
      format.pdf do
        render :pdf => "proposal", :template => "proposals/show"
      end
    end

  end

  def guest_show
    #get the token from url
    token = params[:token]
    #Get the client based on token
    #@client = current_client(token)
    # check if client exists
    #find the corresponding Proposal to the provided token
    @proposal = Proposal.find_by(token: token)
    if !@proposal.nil?
      render json: Proposal.includes(
                  interior_scope: [{room_scopes: [:room_items]}],
                  exterior_scope: [{side_scopes: [:side_items]}]
      ).find_by(token: token)
    else
      render json: { errors: 'Unknown proposal identifier' }, status: 422
    end
  end

  def index
    @proposals = Proposal.all
    render json: Proposal.all, status: 200
  end

  def create
    @estimate = Estimate.find(params[:estimate_id])
    proposal = @estimate.proposals.build(proposal_params[:proposal])
    if proposal.save
      render json: proposal, status: 201
    else
      render json: { errors: proposal.errors }, status: 422
    end
  end

  def update
    proposal = Proposal.find(params[:id])
    if Proposal.update(proposal_params)
      render json: proposal, status: 200
    else
      render json: { errors: proposal.errors }, status: 422
    end
  end

  def destroy
    proposal = Proposal.find(params[:id])
    proposal.destroy
    head 204
  end

  def submit
    @proposal = Proposal.find(params[:proposal_id])
    token = @proposal.token
    id = @proposal.id

    to = params[:email][:to]
    cc = params[:email][:cc]
    subject = params[:email][:subject]
    body = params[:email][:body]
    email = {:to => to, :cc => cc, :subject => subject, :body => body}

    #to be modified to match the production url
    url = "http://127.0.0.1:8080/?#/proposals/#{id}?account=#{@account.name}&token=#{token}"
    ProposalMailer.submit_proposal(url, email).deliver
  end

  def render_pdf
    @proposal = Proposal.find(params[:id])
    respond_to do |format|
      format.pdf { render template: 'proposals/proposal', pdf: 'proposal'}
    end
  end

  private

  def proposal_params
    params.permit(:estimate_id, proposal: [:id,
      :title, :status, :submit_date, :amount, :version,
      interior_scope_attributes: [:materials, :labor, :total,
      room_scopes_attributes: [:name, :materials, :labor, :subtotal,
      room_items_attributes: [:name, :quantity, :coats, :cost]]],
      exterior_scope_attributes: [:materials, :labor, :total,
      side_scopes_attributes: [:code, :materials, :labor, :subtotal,
      side_items_attributes: [:name, :quantity, :coats, :cost]]],
      inclusions_attributes: [:description],
      exclusions_attributes: [:description]
    ])
  end

  def current_client(token)
    @current_client ||= Client.find_by(token: token)
  end

end
