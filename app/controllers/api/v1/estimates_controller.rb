class Api::V1::EstimatesController < ApplicationController
  before_action :authenticate, only: [:set_auth_token]
  before_action :authenticate_with_token!, except: [:authorize, :ionic_authorize]

  def index
    page_info = set_page_info(params)

    @estimates = Estimate.includes(property: [:client]).order('start_time ASC')
                         .page(page_info[:page]).per(page_info[:per_page])
    count = Estimate.count

    pagination = set_pagination(@estimates, page_info, count)

    render json: @estimates, :meta => {:pagination => pagination}
  end

  def show
    @estimate = Estimate.includes(:takeoff, property: [:client])
                         .find(params[:id])

    render json: @estimate
  end

  def create
    @estimate = Estimate.new(estimate_params)
    if @estimate.save
      set_event

      $calendar.get_credentials

      event_link = create_calender_event

      # Set Initails for mailer
      client_full_name = @estimate.property.client.full_name
      client_email = @estimate.property.client.email
      start_time = @estimate.start_time
      current_user = @current_user

      mail_client(current_user, client_email, client_full_name, start_time, event_link)

      render json: @estimate, status: 201
    else
      render json: { errors: @estimate.errors }, status: 422
    end
  end

  def create_calender_event
    link = $calendar.new_event(@event)
  end

  def update
    @estimate = Estimate.find(params[:id])
    if @estimate.update(estimate_params)
      render json: @estimate, status: 201
    else
      render json: { errors: @estimate.errors }, status: 422
    end
  end

  def destroy
    estimate = Estimate.find(params[:id])
    estimate.destroy
    head 204
  end

  def authorize
    $calendar.init_authorizer('angular')
    puts "=====> old credentials: #{$calendar.get_credentials}"
    puts "==== angular_authorize"
    if $calendar.get_credentials
      render json: { authorized: 'OK' }, status: 200
    elsif request['code'] == nil
      puts "==========> redirect: #{$calendar.authorization_url.inspect}"
      render json: { authorized: 'NOK', redirect_to: $calendar.authorization_url }, status: 200
    else
      $calendar.save_credentials(request['code'])
      puts "====> new credentials: #{$calendar.get_credentials.inspect}"
      redirect_to "#{ENV['ANGULAR_URL']}/sales?code=#{request['code']}"
    end
  end

  def ionic_authorize
    $calendar.init_authorizer('ionic')
    puts "=====> old credentials: #{$calendar.get_credentials}"
    puts "==== ionic_authorize"
    if $calendar.get_credentials
      render json: { authorized: 'OK' }, status: 200
    elsif request['code'] == nil
      puts "==========> redirect: #{$calendar.authorization_url.inspect}"
      render json: { authorized: 'NOK', redirect_to: $calendar.authorization_url }, status: 200
    else
      $calendar.save_credentials(request['code'])
      puts "====> new credentials: #{$calendar.get_credentials.inspect}"
      redirect_to ENV['IONIC_URL']
    end
  end


  def search_estimates
    page_info = set_page_info(params)

    result = EstimateModule::FilterEstimateService.call(estimate_params)

    count = result[:estimates].count
    @estimates = result[:estimates]
                  .page(page_info[:page])
                  .per(page_info[:per_page])

    pagination = set_pagination(@estimates, page_info, count) if @estimates

    render json: @estimates, :meta => { :pagination => pagination }
  end

  # ==============================Private Methods=================================
  private

  def estimate_params
    params.require(:estimate)
      .permit(:start_time,
              :end_time,
              :work_description,
              :work_type,
              :status,
              :property_id,
              :arrival,
              :query)
  end

  def set_page_info(params)
    {
      page: params[:page] || 1,
      per_page: params[:per_page] || 10
    }
  end

  def set_pagination(estimates, page_info, count)
   {
    page:        estimates.current_page,
    total_pages: estimates.total_pages,
    next_page:   estimates.next_page,
    prev_page:   estimates.prev_page,
    first_page:  estimates.first_page?,
    last_page:   estimates.last_page?,
    per_page:    page_info[:per_page],
    total:       count}
  end

  def set_event
    property = @estimate.property
    client = property.client
    @event = {
      summary: client.full_name + ' Painting Estimate',
      location: property.full_address,
      description: 'Meet with client and share an estimate',
      start: {
        date_time: @estimate.start_time.strftime("%Y-%m-%dT%H:%M:%S%:z")
        #time_zone: @account.time_zone,
      },
      end: {
          date_time: (@estimate.start_time + 1.hour).strftime("%Y-%m-%dT%H:%M:%S%:z")
          #time_zone: @account.time_zone,
        },
      attendees: [
        {email: client.email},
        {email: @current_user.email},
      ],
      reminders: {
        use_default: true
      },
      send_notifications: true
    }
  end

  def mail_client(current_user, client_email, client_full_name, start_time, event_link)
    EstimateMailer
     .submit_estimate(current_user, client_email, client_full_name, start_time, event_link)
     .deliver
  end
end
