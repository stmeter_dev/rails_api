class Api::V1::TakeoffsController < ApplicationController
  before_action :set_estimate
  before_action :set_takeoff, only: [:update, :destroy]

  # GET /takeoffs
  def index
    @takeoff = @estimate.takeoff
    #render json: Takeoff.includes(:rooms, :sides).find(estimate_id: params[:estimate_id])
    render json: @takeoff
  end

  # GET /takeoffs/1
  # def show
  #   @takeoff = @estimate.takeoff
  #   render json: Takeoff.includes(:rooms, :sides).find(estimate_id: params[:estimate_id], takeoff_id: params[:id])
  # end

  # POST /takeoffs
  def create
    @takeoff = @estimate.build_takeoff(takeoff_params)
    if @takeoff.save
      render json: @takeoff, status: :created
    else
      render json: @takeoff.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /takeoffs/1
  def update
    if(@takeoff.status == 'completed')
      @estimate.update(status: 'completed')
    end
    if @takeoff.update(takeoff_params)
      render json: @takeoff
    else
      render json: @takeoff.errors, status: :unprocessable_entity
    end
  end

  # DELETE /takeoffs/1
  def destroy
    @takeoff.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_estimate
      @estimate = Estimate.find(params[:estimate_id])
    end

    def set_takeoff
      @takeoff = Takeoff.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def takeoff_params
      params.require(:takeoff).permit(:start, :end, :status, :saved, :work_hours,
                :job_labor_cost, :job_materials, :job_material_cost, :job_total_cost, :job_profit,
                :job_tax, :job_price, :rooms, :sides)
    end
end
