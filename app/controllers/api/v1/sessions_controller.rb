class Api::V1::SessionsController < ApplicationController
  def create
    user_password = params[:session][:password]
    user_email = params[:session][:email]
    user = user_email.present? && User.find_by(email: user_email)
    if user != nil
      if user.valid_password? user_password
        sign_in user, store: false
        if(!user.auth_token)
          user.generate_authentication_token!
        end
        user.save
        # Should be done using serialzer, 
        # but for some reason serialzer is not working hence 
        # this approach -voidzero
        user = user.as_json
                   .merge("is_owner": user.is_owner(@account),"signInCount": user.sign_in_count )

        render json: {user: user, account: @account}, status: 200
      else
        render json: { errors: "Invalid email or password" }, status: 422
      end
    else
      render json: { errors: "Unknown user" }, status: 401
    end
  end

  def show
    user = User.find_by(auth_token: params[:id])
    if user
      render json: {data: "success"}, status: 200
    else
      render json: {errors: "invalid token"}, status: 404
    end
  end
  

  
  def destroy
    user = User.find_by(auth_token: params[:id])
    if user
      # user.generate_authentication_token!
      user.auth_token = nil
      user.save
      head 204
    else
      render json: {errors: "Session does not exist!"}, status: 422
    end
  end
end
