class Api::V1::SidesController < ApplicationController
  before_action :authenticate_with_token!
  before_action :find_property, only: [:create, :update, :destroy]

  def create
    sides = []
    errors = []
    side_params[:side].each do |s|
      side = @property.sides.new(s)
      if side.save
        sides << side
      else
        errors << side.errors
      end
    end
    if errors.empty?
      render json: sides, status: 200
    else
      render json: errors, status: 422
    end
   end

  def update
    side = Side.find(params[:id])
    if side.update(side_params[:side])
      render json: side, status: 200
    else
      render json: { errors: side.errors }, status: 422
    end
  end

  def destroy
    side = Side.find(params[:id])
    @property.sides.destroy(side)
    head 204
  end

  private

  def side_params
    params.permit(:id, :property_id, side: [:id, :code,
                  side_features_attributes: [:id, :code, :quantity]])
  end

  def find_property
    @property = Property.find(params[:property_id])
  end

end
