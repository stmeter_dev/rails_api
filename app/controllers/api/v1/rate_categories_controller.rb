class Api::V1::RateCategoriesController < ApplicationController
  before_action :set_rate_category, only: [:show, :update, :destroy]

  # GET /rate_categories
  def index
    @rate_categories = RateCategory.includes(:rates)
    render json: @rate_categories
  end

  # GET /rate_categories/1
  def show
    render json: @rate_category
  end

  # POST /rate_categories
  def create
    @rate_category = RateCategory.new(rate_category_params)

    if @rate_category.save
      render json: @rate_category, status: :created
    else
      render json: @rate_category.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /rate_categories/1
  def update
    if @rate_category.update(rate_category_params)
      render json: @rate_category
    else
      render json: @rate_category.errors, status: :unprocessable_entity
    end
  end

  # DELETE /rate_categories/1
  def destroy
    @rate_category.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rate_category
      @rate_category = RateCategory.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def rate_category_params
      params.require(:rate_category).permit(:name, :inout)
    end
end
