class Api::V1::RoomsController < ApplicationController
  before_action :authenticate_with_token!
  before_action :find_property, only: [:create, :update, :destroy, :index]

  def index
    render json: @property.rooms.all, status: 200
  end

  def create
    rooms = []
    errors = []
    room_params[:room].each do |r|
      room = @property.rooms.new(r)
      if room.save
        rooms << room
      else
        errors << room.errors
      end
    end
    if errors.empty?
      render json: rooms, status: 200
    else
      render json: errors, status: 422
    end
  end

  def update
    room = Room.find(params[:id])
    if room.update(room_params[:room])
      render json: room, status: 200
    else
      render json: { errors: room.errors }, status: 422
    end
  end

  def destroy
    room = Room.find(params[:id])
    @property.rooms.destroy(room)
    head 204
  end

  private

  def room_params
    params.permit(:id, :property_id, room: [:id, :name, :length, :width, :height,
                  :takeoff_id, :hours, :material_quantity, :labor_cost, :material_cost,
                  :number, :material_id, :total_cost, :painted_before, room_features_attributes:
                  [:id, :name, :quantity, :hours, :material_quantity, :labor_cost, :material_cost,
                   :material_id, feature_items_attributes: [:id, :name, :quantity, :hours,
                   :coats, :prep, :material_quantity, :material_id, :width, :height,
                   :note, :pic_url, :rate_id, :rate_category_id]]])
  end

  def find_property
    @property = Property.find(params[:property_id])
  end

end
