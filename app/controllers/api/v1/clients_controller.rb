class Api::V1::ClientsController < ApplicationController
  # after_action :cors_set_access_control_headers, only: [:set_auth_token]
  before_action :authenticate, only: [:new_event]
  before_action :authenticate_with_token!

  def show
    render json: Client.find(params[:id])
  end

  def index
    render json: Client.includes(:properties)
  end

  def create
    @client = Client.new(client_params)
    event
    if @client.save
      
      render json: @client, status: 201
      #new_event
    else
      render json: { errors: @client.errors }, status: 422
    end
  end

  def new_event
    $calendar.new_event(event)
  end


  def update
    @client = Client.find(params[:id])
    if @client.update(client_params)
      render json: @client, status: 200
    else
      render json: { errors: @client.errors }, status: 422
    end
  end

  def destroy
    @client = Client.find(params[:id])
    @client.destroy
    head 204
  end

  def set_auth_token
    #byebug
    if request['code'] == nil
      render json: { redirect_to: $calendar.authorization_url }, status: 200
      #redirect_to $calendar.authorization_url, status: 307
    else
      $calendar.save_credentials(request['code'])
      new_event
    end
  end

  private

  def client_params
    params.require(:client).permit(:first_name, :last_name, :email, :phone_number,
          properties_attributes: [:id, :addr_line1, :addr_line2, :zipcode, :city, :state, :_destroy])
  end

  def authenticate
    #byebug
    unless $calendar.get_credentials
      set_auth_token
    end
  end

  # def cors_set_access_control_headers
  #   #byebug
  #   headers['Access-Control-Allow-Origin'] = '*'
  #   headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE, OPTIONS'
  #   headers['Access-Control-Allow-Headers'] = 'Origin, Content-Type, Accept, Authorization, Token'
  #   headers['Access-Control-Max-Age'] = "1728000"
  # end

  def event
    @event = {
      summary: @client.first_name + ' '  + @client.last_name + 'Painting Estimate',
      location: 'Some location',
      description: 'Meet with client and share an estimate',
      start: {
        date_time: '2017-08-13T17:00:00-07:00',
        time_zone: 'America/Los_Angeles',
      },
      end: {
          date_time: '2017-08-13T17:00:00-08:00',
          time_zone: 'America/Los_Angeles',
        },
      attendees: [
        {email: 'gopalshimpi@gmail.com'},
        {email: 'gopal@aviabird.com'},
      ],
      reminders: {
        use_default: true
      },
      send_notifications: true
    }
  end
end
