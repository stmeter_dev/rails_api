class Api::V1::InteriorScopesController < ApplicationController

  before_action :authenticate_with_token!

  def show
    render json: InteriorScope.find(params[:id])
  end

  def create
    interior_scope = InteriorScope.new(interior_scope_params)
    if proposal.save
      render json: proposal, status: 201
    else
      render json: { errors: proposal.errors }, status: 422
    end
  end

  def update
    proposal = Proposal.find(params[:id])
    if Proposal.update(proposal_params)
      render json: proposal, status: 200
    else
      render json: { errors: proposal.errors }, status: 422
    end
  end

  def destroy
    proposal = Proposal.find(params[:id])
    proposal.destroy
    head 204
  end

  def submit
    proposal = Proposal.find(params[:proposal_id])
    ProposalMailer.submit_proposal(proposal).deliver_later
  end

  private

  def proposal_params
    params.require(:proposal).permit(
                    :estimate_id, :title, :status, :submit_date, :amount,
                    inclusions_attributes: [:id, :description],
                    exclusions_attributes: [:id, :description],
                    interior_scopes_attributes: [:id, :room_id],
                    exterior_scopes_attributes: [:id, :side_id])
  end

end
