class Api::V1::GlobalRatesController < ApplicationController
  before_action :authenticate_with_token!
  before_action :set_global_rate, only: [:show, :update, :destroy]

  # GET /global_rates
  def index
    @global_rates = GlobalRate.all

    render json: @global_rates
  end

  # GET /global_rates/1
  def show
    render json: @global_rate
  end

  # POST /global_rates
  def create
    @global_rate = GlobalRate.new(global_rate_params)

    if @global_rate.save
      render json: @global_rate, status: :created
    else
      render json: @global_rate.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /global_rates/1
  def update
    if @global_rate.update(global_rate_params)
      render json: @global_rate
    else
      render json: @global_rate.errors, status: :unprocessable_entity
    end
  end

  # DELETE /global_rates/1
  def destroy
    @global_rate.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_global_rate
      @global_rate = GlobalRate.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def global_rate_params
      params.require(:global_rate).permit(:name, :value, :category)
    end
end
