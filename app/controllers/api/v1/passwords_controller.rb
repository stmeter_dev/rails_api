class Api::V1::PasswordsController < Devise::PasswordsController
  # GET /resource/password/new
  # def new
  #   super
  # end

  # POST /resource/password
  # def create
  #   super
  # end

  # GET /resource/password/edit?reset_password_token=abcdef
  def edit
    # render json: { ok: "forgot_password" }, status: 200
    redirect_to "http://localhost:4200/updatePassword?reset_token=#{params['reset_password_token']}" 
  end
# 
  # PUT /resource/password
  def update
  end


  private

  def reset_params
  	params.require(:user).permit(:password, :password_confirmation, :reset_password_token)
  end


  # protected

  # def after_resetting_password_path_for(resource)
  #   super(resource)
  # end

  # The path used after sending reset password instructions
  # def after_sending_reset_password_instructions_path_for(resource_name)
  #   super(resource_name)
  # end
end
