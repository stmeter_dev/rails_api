class Api::V1::UsersController < ApplicationController
  before_action :authenticate_with_token!, only: [:invite_user]
  before_action :authenticate_with_token!, only: [:update, :destroy, :index]

  def index
    email = params[:email]
    user = User.all
    render json: {user: user}, status: 200
  end

  def show
    render json: User.find(params[:id])
  end

  def create
    user = User.new(user_params)
    if user.save
      # save the association to account_users table
      create_account_user(user_params[:email])
      ######
      render json: user, status: 201
    else
      render json: { errors: user.errors }, status: 422
    end
  end
  
  def update
    user = current_user
    if user.update(user_params)
      render json: user, status: 200
    else
      render json: { errors: user.errors }, status: 422
    end
  end

  def update_password
    user = current_user
    if user.valid_password?(user_change_password_params[:current_password])
      if user.update(user_change_password_params.except(:current_password))
        bypass_sign_in(user)
        render json: user, status: 200
      else
        render json: { errors: user.errors }, status: 422
      end
    else
      render json: { errors: user.errors }, status: 422
    end
  end  

  def set_timezone
    user = current_user
    if user.update(user_timezone_params)
      render json: user, status: 200
    else
      render json: { errors: user.errors }, status: 422
    end  
  end

  def destroy
    current_user.destroy
    head 204
  end

  def generate_password_email
    email = params[:email]
    @account = check_user(email)
    if @account
      Apartment::Tenant.switch!(@account.name)
      user = User.find_by email: email
      user.send_reset_password_instructions
      render json: { ok: 'success' }, status: 200
    else
      render json: {error: "email doesn't exist"}, status: 400
    end
  end

  def change_password_by_reset_token
    account_name = AccountUser.find_by(email: user_params[:email])
                              .try(:account)
    switch_tenant(account_name)

    attributes = {
      reset_password_token: user_params[:reset_password_token],
      password: user_params[:password],
      password_confirmation: user_params[:password_confirmation]
    }

    user = User.reset_password_by_token(attributes)
    if user.valid?
      render json: { ok: 'success' }, status: 200
    else
      render json: {error: "error while resetting password"}, status: 400
    end
  end

  def invite_user
    email = params[:email]
    user = User.invite!({ email: email }, current_user) do |u|
      u.skip_invitation = true
    end
    create_account_user(user_params[:email])
    user.deliver_invitation
    render json: { ok: 'success' }, status: 200
  end

  def user_confirmation
    @email = params[:email]
    @account = check_user(@email)
    if @account
      Apartment::Tenant.switch!(@account.name)
      user = User.find_by email: @email
      user = User.confirm_by_token(params[:confirmation_token]) 
      @htmldoc = File.read("app/views/users/user_confirmation.html.erb") 
      render html: @htmldoc.html_safe ,status: 200 
    else
        render json: {error: "email doesn't exist"}, status: 400
    end
  end
  
  def accept_invite
    account_name = AccountUser.find_by(email: user_params[:email])
                              .try(:account)
    switch_tenant(account_name)

    user = accept_invitation_and_return_user

    if user.valid?
      render json: { ok: 'success' }, status: 200
    else
      render json: { error: 'error occured' }, status: 400
    end
  end


  # =======================Private Methods=======================
  private

  def user_params
    params.require(:user).permit(
      :email, :name, :password, 
      :password_confirmation, 
      :time_zone, 
      :invitation_token, 
      :reset_password_token,
      :sign_in_count,
      :confirmation_token,
      :confirmed_at)
  end

  def user_change_password_params
    params.require(:user).permit(:current_password, :password, :password_confirmation)
  end

  def user_timezone_params
    params.require(:user).permit(:time_zone)
  end 
  
  def check_user(email)
    account_user = ActiveRecord::Base.connection.execute(
        "SELECT account FROM account_users WHERE email='#{email}';")
    if !account_user.first.nil?
      @account = Account.find_by(name: account_user.first['account'])
    else
      @account = nil
    end
  end

  # Create AccounUser in public schema
  def create_account_user(email)
    Apartment::Tenant.switch!('public')
    ActiveRecord::Base.connection.execute(
      "INSERT INTO account_users (email, account)
       VALUES ('#{email}', '#{current_account.name}');")
    Apartment::Tenant.switch!(current_account.name)
  end

  def switch_tenant(account_name)
    Apartment::Tenant.switch!(account_name)
  end

  def accept_invitation_and_return_user
    User.accept_invitation!(invitation_token: user_params[:invitation_token],
      password: user_params[:password], 
      name: user_params[:name])
  end
end
