class ApplicationController < ActionController::API
  #protect_from_forgery with: :null_session
  include ActionController::MimeResponds
  include Authenticable

  before_action :load_schema
  around_action :set_time_zone
  before_action :set_mailer_host

  private

   def set_mailer_host
      ActionMailer::Base.default_url_options[:host] = request.host_with_port
   end

  def set_time_zone(&block)
    time_zone = @account.try(:time_zone) || 'UTC'
    Time.use_zone(time_zone, &block)
  end

  def load_schema
    puts "=====> in load_schema"
    Apartment::Tenant.switch!('public')
    if current_account
      Apartment::Tenant.switch!(current_account.name)
    else
      Apartment::Tenant.switch!('public')
      #render json: { errors: "Invalid Account Identified" }, status: 403
    end
  end

  def current_account
    puts "=======> in current_account"
    if !request.headers['X-Account'].nil?
      @account = Account.find_by(name: request.headers['X-Account'])
    elsif params[:session]
        #Apartment::Tenant.switch!('public')
        email = params[:session][:email]
        account_users = ActiveRecord::Base.connection.execute(
        "SELECT account FROM account_users WHERE email='#{email}';")
        if !account_users.first.nil?
          @account = Account.find_by(name: account_users.first['account'])
        else
          @account = nil
        end
    else
        @account = nil
    end
  end
end
