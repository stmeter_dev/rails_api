class ProposalMailer < ApplicationMailer
#  Subject can be set in your I18n file at config/locales/en.yml
 # with the following lookup:
  
  #  en.proposal_mailer.submit_proposal.subject
  
  def submit_proposal(url, email)
    @email = email
    @url = url
    #@client = @proposal.estimate.client

    #using MailGun to deliver the email content
    mg_client = Mailgun::Client.new ENV['MG_API_KEY']
    message_params = {
      :from    => 'k.charaa@gmail.com',
      :to      => @email,
      :subject => 'Hello World',
      :html    => (render_to_string(template: "../views/proposal_mailer/submit_proposal")).to_str
    }
    mg_client.send_message ENV['MG_DOMAIN'], message_params
    puts "Email has been sent"

    #using Rails default Mailer
    #mail(to: @email[:to], subject: @email[:subject], template_name: "submit_proposal")
  end
end
