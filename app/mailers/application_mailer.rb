class ApplicationMailer < ActionMailer::Base
  default from: 'k.charaa@gmail.com'
  layout 'mailer'
end
