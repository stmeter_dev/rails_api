 class EstimateMailer < ApplicationMailer

 	def submit_estimate(user, email, cname, estimatedate, event_link)
  	@email = email
  	@name = cname
  	@estimatedate = estimatedate
  	@event_link = event_link
    @current_user = user
 	  # 	mail(to: @email,
    #   	subject: 'Stmeter Painting Estimate' ,
    #   	template_path: "../views/estimate_mailer")

    #using MailGun to deliver the email content
    mg_client = Mailgun::Client.new ENV['MG_API_KEY']
    message_params = {
      :from    => @current_user.email,
      :to      => @email,
      :subject => 'Estimate meeting event',
      :html    => (render_to_string(template: "../views/estimate_mailer/submit_estimate")).to_str
    }
    mg_client.send_message ENV['MG_DOMAIN'], message_params
	end
 end
