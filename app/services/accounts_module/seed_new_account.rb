# Call => AccountsModule::SeedNewAccount.call
# AccountsModule::SeedNewAccount.rates_create
module AccountsModule
  class SeedNewAccount < AccountsModule::Base
    def initialize
      @materials_data = [
        {name: 'Benjamin Moore', cov: 500 , cost: 5},
        {name: 'BPP', cov: 500 , cost: 5},
        {name: 'Valspar American', cov: 500 , cost: 5},
        {name: 'Dunn-Edwards', cov: 500 , cost: 5},
        {name: 'Pratt & Lambert', cov: 500 , cost: 5},
        {name: 'Glidden', cov: 500 , cost: 5},
        {name: 'Sherwin-Williams', cov: 500 , cost: 5}
      ]

      @globalrates_data = [
        {name: 'profit', value: 25, 'category': 'percent'},
        {name: 'labor', value: 50, 'category': 'num'},
        {name: 'tax', value: 8, 'category': 'percent'}
      ]

      @flexrates_data = [
        {name: 'Wall', rate: 1, unit: 'Hr/100 SQF'},
        {name: 'Ceiling', rate: 1.5, unit: 'Hr/100 SQF'},
        {name: 'Window', rate: 1, unit: 'ITEM'},
        {name: 'Door', rate: 2, unit: 'ITEM'},
        {name: 'Trim', rate: 1, unit: 'Hr/100 LF'}
      ]

      @ratecategories_data = [
        {name: 'Walls', inout: 'IN'},
        {name: 'Ceiling', inout: 'IN'},
        {name: 'Windows', inout: 'IN'},
        {name: 'Doors', inout: 'IN'},
        {name: 'Base', inout: 'IN'},
        {name: 'Crown', inout: 'IN'},
        {name: 'Trim', inout: 'IN'},
        {name: 'Stairs', inout: 'IN'},
        {name: 'Rails', inout: 'IN'},
        {name: 'Cabinetry', inout: 'IN'},
        {name: 'Handrail', inout: 'IN'},
        {name: 'Skim Wall', inout: 'IN'},
        {name: 'Skim Ceiling', inout: 'IN'}
      ]

      @rates_data = [
        {name: 'Flat Wall', unit: 'SQF/Hr', first_coat: 175, second_coat: 220, rate_category_id: 1},
        {name: 'Enamel Wall', unit: 'SQF/Hr', first_coat: 150, second_coat: 175, rate_category_id: 1},
        {name: 'Ceiling', unit: 'SQF/Hr', first_coat: 185, second_coat: 200, rate_category_id: 2},
        {name: 'Base', unit: 'SQF/Hr', first_coat: 40, second_coat: 40, rate_category_id: 5},
        {name: 'Crown', unit: 'SQF/Hr', first_coat: 25, second_coat: 40, rate_category_id: 6},
        {name: 'Crafts-Style Trim', unit: 'LF/Hr', first_coat: 25, second_coat: 40, rate_category_id: 7},
        {name: 'Flush Door', unit: 'Hr/Unit', first_coat: 2, second_coat: 2, rate_category_id: 4},
        {name: '1 Lite Door', unit: 'Hr/Unit', first_coat: 1.5, second_coat: 1.5, rate_category_id: 4},
        {name: '1 + Panels Door', unit: 'SQF/Hr', first_coat: 75, second_coat: 75, rate_category_id: 4},
        {name: '1 Lite Slider Door', unit: 'SQF/Hr', first_coat: 90, second_coat: 90, rate_category_id: 4},
        {name: '1 Lite Window-Small', unit: 'Hr/Unit', first_coat: 1, second_coat: 1, rate_category_id: 3},
        {name: '1 Lite Window-Medium', unit: 'Hr/Unit', first_coat: 1.5, second_coat: 1.5, rate_category_id: 3},
        {name: '1 Lite Window-Large', unit: 'Hr/Unit', first_coat: 2, second_coat: 2, rate_category_id: 3},
        {name: 'French Lite Window', unit: 'LF/Hr', first_coat: 20, second_coat: 20, rate_category_id: 3},
        {name: 'Narrow Trim', unit: 'LF/Hr', first_coat: 70, second_coat: 70, rate_category_id: 7}
      ]

      @productionrates_data = [
        {name: 'Wall-F0-8', rate: 175, unit: 'Sf/hr', category: 'INT'},
        {name: 'Wall-F9-11', rate: 135, unit: 'Sf/hr', category: 'INT'},
        {name: 'Ceiling-0-8', rate: 185, unit: 'Sf/hr', category: 'INT'},
        {name: 'Base', rate: 40, unit: 'Sf/hr', category: 'INT'},
        {name: 'Crown', rate: 25, unit: 'Sf/hr', category: 'INT'},
        {name: 'Door', rate: 75, unit: 'Sf/hr', category: 'INT'},
        {name: 'Window', rate: 65, unit: 'Ft/hr', category: 'INT'}
      ]

      @measures_data = [
        {name: 'Flush', value: 40, unit: 'SF', category: 'Doors'},
        {name: '1 Lite', value: 55, unit: 'SF', category: 'Doors'},
        {name: '1 + Panels', value: 70, unit: 'SF', category: 'Doors'},
        {name: '10 Lite 7`', value: 70, unit: 'SF', category: 'Doors'},
        {name: '10 Lite 8`', value: 85, unit: 'SF', category: 'Doors'},
        {name: '1 Lite Slider Unit', value: 150, unit: 'SF', category: 'Doors'},
        {name: '1 Lite New Slider', value: 150, unit: 'SF', category: 'Doors'},
        {name: 'Frame only', value: 30, unit: 'SF', category: 'Doors'},

        {name: 'Window Sills', value: 5, unit: 'LF', category: 'Windows'},
        {name: 'Small Wnd Frame', value: 25, unit: 'LF', category: 'Windows'},
        {name: '1 Lite Windows: Small', value: 35, unit: 'LF', category: 'Windows'},
        {name: '1 Lite Windows: Large', value: 45, unit: 'LF', category: 'Windows'},
        {name: 'French 2-7 Lite', value: 55, unit: 'LF', category: 'Windows'},
        {name: 'French 8-12 Lite', value: 70, unit: 'LF', category: 'Windows'},
        {name: 'French 12-16 Lite', value: 85, unit: 'LF', category: 'Windows'},
        {name: 'Large French 16+ Lites', value: 100, unit: 'LF', category: 'Windows'},

        {name: 'Basic Fireplace', value: 50, unit: 'LF', category: 'Trim Work'},
        {name: 'Basic Mantel', value: 15, unit: 'LF', category: 'Trim Work'},
        {name: 'Detailed Fireplace', value: 100, unit: 'LF', category: 'Trim Work'},
        {name: 'Plantation Shutters', value: 6, unit: 'LF', category: 'Trim Work'}
      ]
    end

    def call
      begin
          materials_create
          globalrates_create
          flexrates_create
          ratecategories_create
          rates_create
          productionrates_create
          measures_create
      rescue Exception => e
        return e
      end
    end

    def materials_create
      if Material.count == 0
        @materials_data.each do |material|
          Material.create!(material)
        end
        puts "material seed done"
      end
    end

    def globalrates_create
      if GlobalRate.count == 0
        @globalrates_data.each do |globalrate|
          GlobalRate.create!(globalrate)
        end
        puts "globalrate seed done"
      end
    end

    def flexrates_create
      if FlexRate.count == 0
        @flexrates_data.each do |flexrate|
          FlexRate.create!(flexrate)
        end
        puts "flexrate seed done"
      end
    end

    def rates_create
      if Rate.count == 0
        @rates_data.each do |rate|
          Rate.create!(rate)
        end
        puts "rate seed done"
      end
    end

    def ratecategories_create
      if RateCategory.count == 0
        @ratecategories_data.each do |ratecategory|
          RateCategory.create(ratecategory)
        end
        puts "ratecatergory seed done"
      end
    end

    def productionrates_create
      if ProductionRate.count == 0
        @productionrates_data.each do |productionrate|
          ProductionRate.create(productionrate)
        end
        puts "productionrate seed done"
      end
    end

    def measures_create
      if Measure.count == 0
        @measures_data.each do |measure|
          Measure.create(measure)
        end
        puts "material seed done"
      end
    end

  end
end
