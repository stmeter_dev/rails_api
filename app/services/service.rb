class Service
	def call(*_args)
		self
	end

	def self.call(*args)
		new(*args).call
	end

	def missing_parameter(obj_name)
		raise ArgumentError,"Invalid or Missing #{obj_name} Object"
	end
end