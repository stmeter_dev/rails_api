# Call:
# => search_params = { arrival: 'all', status: 'pending', query: '' }  
# => EstimateModule::FilterEstimateService.call(search_params)

module EstimateModule
  class FilterEstimateService < EstimateModule::Base
    # search_params: {arrival: '', status: '', query: ''}
    def initialize(search_params)
      @search_params = search_params
      @date = get_current_date
      @start_date = Estimate.order(:start_time).first.start_time.beginning_of_day 
      @end_date = Estimate.order(:start_time).last.start_time.end_of_day
      @estimates = [];
    end

    # Search on Arrival criteria
    # Search on Staus criteria based on arrival results
    # Search on query 
    def call
      set_start_and_end_date
      get_estimates_based_on_arrival
      get_estimates_based_on_status if @estimates
      get_estimates_based_on_query if @estimates
      return { status: :ok, estimates: @estimates }
    end

    private 

    def get_current_date
      Time.now
    end

    def set_start_and_end_date
      
      arrival_type = @search_params[:arrival]

      if arrival_type == 'today'
        @start_date = @date.beginning_of_day
        @end_date = @date.end_of_day
      elsif arrival_type == 'week'
        @start_date = @date.beginning_of_week
        @end_date = @date.end_of_week
      elsif arrival_type == 'month'
        @start_date = @date.beginning_of_month
        @end_date = @date.end_of_month
      end
    end

    def get_estimates_based_on_arrival
      @estimates = Estimate
                    .includes(property: [:client])
                    .where(start_time: @start_date..@end_date)
    end

    def get_estimates_based_on_status
      status = @search_params[:status]
      if status == 'pending'
        @estimates = @estimates.where(status: 'pending')
      elsif status == 'completed'
        @estimates = @estimates.where(status: 'completed')
      elsif status =='ongoing'
        @estimates = @estimates.where(status: 'ongoing')
      end
    end

    def get_estimates_based_on_query
      query = @search_params[:query]
      if query.present?
        @estimates = @estimates
                      .joins(property: :client)
                      .where("clients.full_name ~* ?", ".*#{query}.*")
      end      
    end
  end
end