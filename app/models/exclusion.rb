# == Schema Information
#
# Table name: exclusions
#
#  id          :integer          not null, primary key
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  proposal_id :integer
#

class Exclusion < ApplicationRecord
  belongs_to :proposal
end
