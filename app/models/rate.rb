# == Schema Information
#
# Table name: rates
#
#  id               :integer          not null, primary key
#  rate_category_id :integer
#  name             :string
#  first_coat       :integer
#  second_coat      :integer
#  third_coat       :integer
#  fourth_coat      :integer
#  description      :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Rate < ApplicationRecord
  belongs_to :rate_category
end
