# == Schema Information
#
# Table name: side_items
#
#  id            :integer          not null, primary key
#  side_scope_id :integer
#  name          :string
#  quantity      :integer
#  coats         :integer
#  cost          :decimal(, )
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class SideItem < ApplicationRecord
  belongs_to :side_scope
end
