# == Schema Information
#
# Table name: side_scopes
#
#  id                :integer          not null, primary key
#  exterior_scope_id :integer
#  materials         :decimal(, )
#  labor             :decimal(, )
#  subtotal          :decimal(, )
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  code              :string
#

class SideScope < ApplicationRecord
  belongs_to :exterior_scope
  has_many   :side_items, inverse_of: :side_scope, dependent: :destroy
  accepts_nested_attributes_for :side_items
end
