# == Schema Information
#
# Table name: clients
#
#  id           :integer          not null, primary key
#  first_name   :string
#  last_name    :string
#  email        :string
#  phone_number :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  full_name    :string
#  user_id      :integer
#

class Client < ApplicationRecord
  before_create :set_fullname
  has_many :properties, inverse_of: :client, :dependent => :delete_all
  accepts_nested_attributes_for :properties

  def set_fullname
    self.full_name = self.first_name.titleize  + ' ' + self.last_name.titleize 
  end
end
