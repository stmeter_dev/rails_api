# == Schema Information
#
# Table name: sides
#
#  id          :integer          not null, primary key
#  code        :string
#  property_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  width       :integer          default(0), not null
#  height      :integer          default(0), not null
#  takeoff_id  :integer
#

class Side < ApplicationRecord
  belongs_to :property
  has_many   :side_features, inverse_of: :side, dependent: :destroy
  accepts_nested_attributes_for :side_features
end
