# == Schema Information
#
# Table name: inclusions
#
#  id          :integer          not null, primary key
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  proposal_id :integer
#

class Inclusion < ApplicationRecord
  belongs_to :proposal
end
