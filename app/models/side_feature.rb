# == Schema Information
#
# Table name: side_features
#
#  id         :integer          not null, primary key
#  code       :string
#  quantity   :integer
#  side_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SideFeature < ApplicationRecord
  belongs_to :side
end
