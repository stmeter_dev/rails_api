# == Schema Information
#
# Table name: flex_rates
#
#  id          :integer          not null, primary key
#  name        :string
#  description :string
#  rate        :decimal(, )
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  unit        :string
#

class FlexRate < ApplicationRecord
  validates :name, :rate, :unit, presence: true
end
