# == Schema Information
#
# Table name: public.account_users
#
#  email   :text
#  account :text
#

class AccountUser < ActiveRecord::Base
  validates_presence_of :email, :account
  validates_uniqueness_of :email
end
