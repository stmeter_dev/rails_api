# == Schema Information
#
# Table name: room_features
#
#  id                :integer          not null, primary key
#  name              :string
#  quantity          :integer
#  room_id           :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  rate_category_id  :integer
#  rate_id           :integer
#  hours             :decimal(, )
#  coats             :integer
#  prep              :integer
#  material_quantity :decimal(, )
#  length            :integer
#  width             :integer
#  comment           :text
#  pic_url           :string
#  material_id       :integer
#  cost              :decimal(, )
#

class RoomFeature < ApplicationRecord
  belongs_to :room
  #belongs_to :rate_category
  has_one    :rate
  has_one    :material
  has_many   :feature_items, inverse_of: :room_feature, dependent: :destroy
  accepts_nested_attributes_for :feature_items
end
