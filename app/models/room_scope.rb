# == Schema Information
#
# Table name: room_scopes
#
#  id                :integer          not null, primary key
#  interior_scope_id :integer
#  materials         :decimal(, )
#  labor             :decimal(, )
#  subtotal          :decimal(, )
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  name              :string
#

class RoomScope < ApplicationRecord
  belongs_to :interior_scope
  #belongs_to :room
  has_many   :room_items, inverse_of: :room_scope, dependent: :destroy
  accepts_nested_attributes_for :room_items
end
