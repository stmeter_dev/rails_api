# == Schema Information
#
# Table name: room_items
#
#  id            :integer          not null, primary key
#  room_scope_id :integer
#  name          :string
#  quantity      :integer
#  coats         :integer
#  cost          :decimal(, )
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class RoomItem < ApplicationRecord
  belongs_to :room_scope
end
