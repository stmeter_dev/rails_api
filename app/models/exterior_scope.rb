# == Schema Information
#
# Table name: exterior_scopes
#
#  id          :integer          not null, primary key
#  proposal_id :integer
#  materials   :decimal(, )
#  labor       :decimal(, )
#  total       :decimal(, )
#

class ExteriorScope < ApplicationRecord
  belongs_to :proposal
  has_many :side_scopes, inverse_of: :exterior_scope, dependent: :destroy
  accepts_nested_attributes_for :side_scopes
end
