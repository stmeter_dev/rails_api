# == Schema Information
#
# Table name: production_rates
#
#  id                     :integer          not null, primary key
#  name                   :string
#  unit                   :string
#  rate                   :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  production_template_id :integer
#  category               :string
#

class ProductionRate < ApplicationRecord
  #belongs_to :production_template, inverse_of: :production_rates
  validates :name, :rate, :unit, :category, presence: true
end
