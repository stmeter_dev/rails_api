# == Schema Information
#
# Table name: materials
#
#  id         :integer          not null, primary key
#  name       :string
#  cov        :decimal(, )
#  cost       :decimal(, )
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Material < ApplicationRecord
  validates :name, :cov, :cost, presence: true
  validates :name, uniqueness: { case_sensitive: false }
end
