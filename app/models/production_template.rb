# == Schema Information
#
# Table name: production_templates
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  active     :boolean          default(TRUE), not null
#  category   :string           default("RES_INT"), not null
#

class ProductionTemplate < ApplicationRecord
  #has_many :production_rates, inverse_of: :production_template, dependent: :destroy
  validates :name, presence: true, uniqueness: true
  validates :category, presence: true
  #accepts_nested_attributes_for :production_rates, allow_destroy: true

  scope :active, -> { where(active: true) }
  scope :inactive, -> { where(active: false)}
end
