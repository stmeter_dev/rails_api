# == Schema Information
#
# Table name: rooms
#
#  id                :integer          not null, primary key
#  name              :string
#  length            :integer
#  width             :integer
#  height            :integer
#  property_id       :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  number            :integer
#  painted_before    :boolean          default(FALSE)
#  takeoff_id        :integer
#  hours             :decimal(, )
#  material_quantity :decimal(, )
#  labor_cost        :decimal(, )
#  material_cost     :decimal(, )
#  material_id       :integer
#

class Room < ApplicationRecord
  belongs_to :property
  has_many   :room_features, inverse_of: :room, dependent: :destroy
  has_many   :room_scopes
  accepts_nested_attributes_for :room_features
end
