# == Schema Information
#
# Table name: properties
#
#  id           :integer          not null, primary key
#  addr_line1   :string
#  addr_line2   :string
#  zipcode      :string
#  city         :string
#  state        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  client_id    :integer
#  full_address :string
#

class Property < ApplicationRecord
  before_create :set_fulladdress
  belongs_to :client
  has_many :rooms, :dependent => :delete_all
  has_many :sides, :dependent => :delete_all

  def set_fulladdress
    self.full_address = self.addr_line1.to_s.titleize + ', ' + self.addr_line2.to_s.titleize + ', ' +
      self.zipcode.to_s + ', ' + self.city.to_s.titleize
  end
end
