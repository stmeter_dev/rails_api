# == Schema Information
#
# Table name: interior_scopes
#
#  id          :integer          not null, primary key
#  proposal_id :integer
#  materials   :decimal(, )
#  labor       :decimal(, )
#  total       :decimal(, )
#

class InteriorScope < ApplicationRecord
  belongs_to :proposal
  has_many :room_scopes, inverse_of: :interior_scope, dependent: :destroy
  accepts_nested_attributes_for :room_scopes
end
