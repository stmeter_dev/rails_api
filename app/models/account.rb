# == Schema Information
#
# Table name: public.accounts
#
#  id           :integer          not null, primary key
#  owner_id     :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  name         :string
#  addr_line1   :string
#  addr_line2   :string
#  zipcode      :string
#  city         :string
#  state        :string
#  phone_number :string
#  cell_number  :string
#  fax          :string
#  logo         :string
#  time_zone    :string
#

class Account < ActiveRecord::Base
  RESTRICTED_NAMES = %w(www)
  belongs_to :owner, class_name: 'User'
  validates :owner, presence: true
  validates :name, presence: true,
            uniqueness: { case_sensitive: false },
            format: { with: /\A[\w\-]+\Z/i, message: 'contains invalid characters' },
            exclusion: { in: RESTRICTED_NAMES, message: 'restricted name' }

  accepts_nested_attributes_for :owner
  before_validation :downcase_name

  private
  def downcase_name
    self.name = name.try(:downcase)
  end
end
