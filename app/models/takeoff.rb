# == Schema Information
#
# Table name: takeoffs
#
#  id                :integer          not null, primary key
#  estimate_id       :integer
#  start             :datetime
#  end               :datetime
#  status            :string
#  saved             :boolean
#  work_hours        :decimal(, )
#  job_labor_cost    :decimal(, )
#  job_material_cost :decimal(, )
#  job_total_cost    :decimal(, )
#  job_profit        :decimal(, )
#  job_tax           :decimal(, )
#  job_price         :decimal(, )
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  sheet_url         :string           default("")
#  job_materials     :float
#

class Takeoff < ApplicationRecord
  belongs_to :estimate
  has_many   :rooms
  has_many   :sides
end
