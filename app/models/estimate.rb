# == Schema Information
#
# Table name: estimates
#
#  id               :integer          not null, primary key
#  start_time       :datetime
#  end_time         :datetime
#  work_description :text
#  property_id      :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  work_type        :string
#  status           :string
#  user_id          :integer
#

class Estimate < ApplicationRecord
  belongs_to :property
  has_one    :takeoff, :dependent => :destroy
  has_many   :proposals, inverse_of: :estimate
  #belongs_to :user
  #belongs_to :client
  #accepts_nested_attributes_for :property, allow_destroy: true
  before_save :downcase_status
  validates :status, inclusion: { in: %w(pending ongoing completed) }
  
  def downcase_status
  	return self.status.downcase!
  end
end
