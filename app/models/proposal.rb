# == Schema Information
#
# Table name: proposals
#
#  id          :integer          not null, primary key
#  estimate_id :integer
#  title       :string
#  status      :string
#  submit_date :datetime
#  amount      :decimal(, )
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  version     :string
#  token       :string
#

class Proposal < ApplicationRecord
  before_create :generate_token

  belongs_to :estimate, inverse_of: :proposals
  has_many :inclusions, inverse_of: :proposal, dependent: :destroy
  has_many :exclusions, inverse_of: :proposal, dependent: :destroy
  has_one :exterior_scope, inverse_of: :proposal, dependent: :destroy
  has_one :interior_scope, inverse_of: :proposal, dependent: :destroy

  accepts_nested_attributes_for :interior_scope, :exterior_scope, :inclusions, :exclusions

  def generate_token
   self.token = Digest::SHA1.hexdigest([self.estimate_id, Time.now, rand].join)
  end
end
