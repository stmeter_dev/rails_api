# == Schema Information
#
# Table name: int_features
#
#  id          :integer          not null, primary key
#  code        :string
#  quantity    :integer
#  property_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class IntFeature < ApplicationRecord
  belongs_to :property
end
