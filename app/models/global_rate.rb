# == Schema Information
#
# Table name: global_rates
#
#  id         :integer          not null, primary key
#  value      :decimal(, )
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  name       :string
#  category   :string
#

class GlobalRate < ApplicationRecord
  validates :name, presence: true,
            uniqueness: { case_sensitive: false }
  validates :value, presence: true
end
