class SideItemSerializer < ActiveModel::Serializer
  attributes :id, :name, :quantity, :coats, :cost
  belongs_to :side_scope
end
