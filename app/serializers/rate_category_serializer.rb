# == Schema Information
#
# Table name: rate_categories
#
#  id         :integer          not null, primary key
#  name       :string
#  unit       :string
#  inout      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class RateCategorySerializer < ActiveModel::Serializer
  attributes :id, :name, :inout
  has_many :rates
end
