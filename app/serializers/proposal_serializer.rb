# == Schema Information
#
# Table name: proposals
#
#  id          :integer          not null, primary key
#  estimate_id :integer
#  title       :string
#  status      :string
#  submit_date :datetime
#  amount      :decimal(, )
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  version     :string
#  token       :string
#

class ProposalSerializer < ActiveModel::Serializer
  attributes :id, :title, :status, :submit_date, :amount, :estimate, :interior_scope, :exterior_scope
  has_one :interior_scope, serializer: InteriorScopeSerializer
  has_one :exterior_scope, serializer: ExteriorScopeSerializer
  belongs_to :estimate
end
