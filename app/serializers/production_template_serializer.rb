# == Schema Information
#
# Table name: production_templates
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  active     :boolean          default(TRUE), not null
#  category   :string           default("RES_INT"), not null
#

class ProductionTemplateSerializer < ActiveModel::Serializer
  attributes :id, :name, :active
  #has_many :production_rates
end
