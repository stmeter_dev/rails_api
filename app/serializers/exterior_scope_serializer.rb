# == Schema Information
#
# Table name: exterior_scopes
#
#  id          :integer          not null, primary key
#  proposal_id :integer
#  materials   :decimal(, )
#  labor       :decimal(, )
#  total       :decimal(, )
#

class ExteriorScopeSerializer < ActiveModel::Serializer
  attributes :id, :materials, :labor, :total, :side_scopes

  def side_scopes
    custom_side_scopes = []
    object.side_scopes.each do |side_scope|
      custom_side_scope = side_scope.attributes
      custom_side_scope[:side_items] = side_scope.side_items.collect{
        |side_item| side_item.slice(:id, :name, :quantity, :coats, :cost)
      }

      custom_side_scopes.push(custom_side_scope)
    end
    return custom_side_scopes
  end
end
