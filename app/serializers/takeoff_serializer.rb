# == Schema Information
#
# Table name: takeoffs
#
#  id                :integer          not null, primary key
#  estimate_id       :integer
#  start             :datetime
#  end               :datetime
#  status            :string
#  saved             :boolean
#  work_hours        :decimal(, )
#  job_labor_cost    :decimal(, )
#  job_material_cost :decimal(, )
#  job_total_cost    :decimal(, )
#  job_profit        :decimal(, )
#  job_tax           :decimal(, )
#  job_price         :decimal(, )
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  sheet_url         :string           default("")
#  job_materials     :float
#

class TakeoffSerializer < ActiveModel::Serializer
  attributes :id, :start, :end, :status, :saved, :work_hours, :job_labor_cost,
  :job_material_cost, :job_materials, :job_total_cost, :job_profit, :job_tax, :job_price, :sheet_url,
  :rooms,  :sides

  def rooms
    rooms = []
    object.rooms.each do |room|
      rooms.push(RoomSerializer.new(room))
    end
    return rooms
  end
  # def rooms
  #   rooms = []
  #   object.rooms.each do |room|
  #     room_attributes = room.attributes
  #     room_attributes[:room_features_attributes] = room.room_features.collect{
  #       |feature| feature.slice(:id, :name, :quantity, :hours, :material_quantity, :labor_cost, :material_cost)
  #     }
  #     room_attributes[:room_features_attributes].each do |feature|
  #       #feature_attributes = feature.attributes
  #       feature[:feature_items_attributes] = feature[:feature_items].collect{
  #         |item| item.slice(:id, :name, :quantity, :hours, :coats, :prep, :material_quantity,
  #                           :material_id, :width, :height, :note, :pic_url, :rate_id, :rate_category_id)
  #       }
  #     end
  #     rooms.push(room_attributes)
  #   end
  #
  #   return rooms
  # end

  def sides
    sides = []
    object.sides.each do |side|
      side_attributes = side.attributes
      side_attributes[:side_features_attributes] = side.side_features.collect{
        |feature| feature.slice(:id, :name, :quantity, :hours, :material_quantity, :coats, :prep)
      }
      sides.push(side_attributes)
    end
    return sides
  end
end
