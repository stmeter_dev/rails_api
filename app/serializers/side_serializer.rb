# == Schema Information
#
# Table name: sides
#
#  id          :integer          not null, primary key
#  code        :string
#  property_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  width       :integer          default(0), not null
#  height      :integer          default(0), not null
#  takeoff_id  :integer
#

class SideSerializer < ActiveModel::Serializer
  attributes :id, :code
  has_many :side_features
end
