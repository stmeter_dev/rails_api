# == Schema Information
#
# Table name: properties
#
#  id           :integer          not null, primary key
#  addr_line1   :string
#  addr_line2   :string
#  zipcode      :string
#  city         :string
#  state        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  client_id    :integer
#  full_address :string
#

class PropertySerializer < ActiveModel::Serializer
  attributes :id, :addr_line1, :addr_line2, :zipcode, :city, :state, :full_address
  belongs_to :client
end
