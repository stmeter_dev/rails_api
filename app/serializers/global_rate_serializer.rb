# == Schema Information
#
# Table name: global_rates
#
#  id         :integer          not null, primary key
#  value      :decimal(, )
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  name       :string
#  category   :string
#

class GlobalRateSerializer < ActiveModel::Serializer
  attributes :id, :name, :value, :category
end
