# == Schema Information
#
# Table name: public.accounts
#
#  id           :integer          not null, primary key
#  owner_id     :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  name         :string
#  addr_line1   :string
#  addr_line2   :string
#  zipcode      :string
#  city         :string
#  state        :string
#  phone_number :string
#  cell_number  :string
#  fax          :string
#  logo         :string
#  time_zone    :string
#

class AccountSerializer < ActiveModel::Serializer
  attributes :id, :name, :addr_line1, :addr_line2, :zipcode, :city,
              :state, :phone_number, :cell_number, :fax, :logo
end
