# == Schema Information
#
# Table name: clients
#
#  id           :integer          not null, primary key
#  first_name   :string
#  last_name    :string
#  email        :string
#  phone_number :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  full_name    :string
#  user_id      :integer
#

class ClientSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :last_name, :email, :phone_number, :full_name
  has_many :properties
end
