# == Schema Information
#
# Table name: production_rates
#
#  id                     :integer          not null, primary key
#  name                   :string
#  unit                   :string
#  rate                   :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  production_template_id :integer
#  category               :string
#

class ProductionRateSerializer < ActiveModel::Serializer
  attributes :id, :name, :rate, :unit, :category
  #belongs_to :production_template
end
