# == Schema Information
#
# Table name: flex_rates
#
#  id          :integer          not null, primary key
#  name        :string
#  description :string
#  rate        :decimal(, )
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  unit        :string
#

class FlexRateSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :rate, :unit
end
