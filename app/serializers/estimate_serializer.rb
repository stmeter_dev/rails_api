# == Schema Information
#
# Table name: estimates
#
#  id               :integer          not null, primary key
#  start_time       :datetime
#  end_time         :datetime
#  work_description :text
#  property_id      :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  work_type        :string
#  status           :string
#  user_id          :integer
#


class EstimateSerializer < ActiveModel::Serializer
  attributes :id, :start_time, :end_time, :work_description, :status, :work_type, :property, :takeoff

  def property
    if !object.property.nil?
      property_attributes = object.property.attributes
      property_attributes[:client] = object.property.client.attributes
      return property_attributes
    else
      return nil
    end
  end

  def takeoff
    if !object.takeoff.nil?
      return TakeoffSerializer.new(object.takeoff);
    end
  end

end
