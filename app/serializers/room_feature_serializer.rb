# == Schema Information
#
# Table name: room_features
#
#  id                :integer          not null, primary key
#  name              :string
#  quantity          :integer
#  room_id           :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  rate_category_id  :integer
#  rate_id           :integer
#  hours             :decimal(, )
#  coats             :integer
#  prep              :integer
#  material_quantity :decimal(, )
#  length            :integer
#  width             :integer
#  comment           :text
#  pic_url           :string
#  material_id       :integer
#  cost              :decimal(, )
#

class RoomFeatureSerializer < ActiveModel::Serializer
  attributes :room_id, :id, :name, :quantity, :rate_category_id, :rate_id,
             :material_id, :hours, :coats, :prep, :material_quantity, :length, :width,
             :comment, :pic_url
end
