# == Schema Information
#
# Table name: rooms
#
#  id                :integer          not null, primary key
#  name              :string
#  length            :integer
#  width             :integer
#  height            :integer
#  property_id       :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  number            :integer
#  painted_before    :boolean          default(FALSE)
#  takeoff_id        :integer
#  hours             :decimal(, )
#  material_quantity :decimal(, )
#  labor_cost        :decimal(, )
#  material_cost     :decimal(, )
#  material_id       :integer
#

class RoomSerializer < ActiveModel::Serializer
  attributes :property_id, :takeoff_id, :id, :name, :length, :width, :height, :number,
             :hours, :material_quantity, :labor_cost, :material_cost, :total_cost, :material_id,
             :room_features_attributes

 def room_features_attributes
   custom_room_features = []
   object.room_features.each do |feature|
     custom_room_feature = feature.attributes
     custom_room_feature[:feature_items_attributes] = feature.feature_items.collect{
       |item| item.slice(:id, :name, :quantity, :hours, :coats, :prep, :material_quantity,
                         :material_id, :width, :height, :note, :pic_url, :rate_id, :rate_category_id)
     }

     custom_room_features.push(custom_room_feature)
   end
   return custom_room_features
 end

end
