# == Schema Information
#
# Table name: interior_scopes
#
#  id          :integer          not null, primary key
#  proposal_id :integer
#  materials   :decimal(, )
#  labor       :decimal(, )
#  total       :decimal(, )
#

class InteriorScopeSerializer < ActiveModel::Serializer
  attributes :id, :materials, :labor, :total, :room_scopes

  def room_scopes
    custom_room_scopes = []
    object.room_scopes.each do |room_scope|
      custom_room_scope = room_scope.attributes
      custom_room_scope[:room_items] = room_scope.room_items.collect{
        |room_item| room_item.slice(:id, :name, :quantity, :coats, :cost)
      }

      custom_room_scopes.push(custom_room_scope)
    end
    return custom_room_scopes
  end

end
