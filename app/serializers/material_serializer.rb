# == Schema Information
#
# Table name: materials
#
#  id         :integer          not null, primary key
#  name       :string
#  cov        :decimal(, )
#  cost       :decimal(, )
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class MaterialSerializer < ActiveModel::Serializer
  attributes :id, :name, :cov, :cost
end
