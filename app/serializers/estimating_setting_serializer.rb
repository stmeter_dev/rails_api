class EstimatingSettingSerializer < ActiveModel::Serializer
  attributes :id, :labor, :profit, :tax
end
