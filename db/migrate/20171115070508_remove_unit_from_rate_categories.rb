class RemoveUnitFromRateCategories < ActiveRecord::Migration[5.0]
  def change
    remove_column :rate_categories, :unit, :string
  end
end
