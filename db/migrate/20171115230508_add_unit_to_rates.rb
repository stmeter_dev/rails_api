class AddUnitToRates < ActiveRecord::Migration[5.0]
  def change
    add_column :rates, :unit, :string
  end
end
