class AddCostToRoomFeature < ActiveRecord::Migration[5.1]
  def self.up
  	add_column :room_features, :cost, :decimal
  end

  def self.down
  	remove_column :room_features, :cost
  end
end
