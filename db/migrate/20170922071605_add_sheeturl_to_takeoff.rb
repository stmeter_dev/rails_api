class AddSheeturlToTakeoff < ActiveRecord::Migration[5.1]
  def change
    add_column :takeoffs, :sheet_url, :string, default: '' 
  end
end
