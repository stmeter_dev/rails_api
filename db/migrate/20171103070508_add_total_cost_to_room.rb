class AddTotalCostToRoom < ActiveRecord::Migration[5.1]
  def change
    add_column :rooms, :total_cost, :decimal
  end
end
