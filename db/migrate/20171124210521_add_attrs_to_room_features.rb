class AddAttrsToRoomFeatures < ActiveRecord::Migration[5.1]
  def change
    add_column :room_features, :labor_cost, :decimal
    add_column :room_features, :material_cost, :decimal
    remove_column :room_features, :rate_category_id, :integer
    remove_column :room_features, :rate_id, :integer
    remove_column :room_features, :coats, :integer
    remove_column :room_features, :prep, :integer
    remove_column :room_features, :length, :integer
    remove_column :room_features, :width, :integer
    remove_column :room_features, :comment, :text
    remove_column :room_features, :pic_url, :string
    remove_column :room_features, :cost, :decimal
  end
end
