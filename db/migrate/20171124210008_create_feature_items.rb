class CreateFeatureItems < ActiveRecord::Migration[5.1]
  def change
    create_table :feature_items do |t|
      t.string :name
      t.decimal :quantity
      t.decimal :hours
      t.decimal :coats
      t.decimal :prep
      t.decimal :material_quantity
      t.integer :material_id
      t.integer :width
      t.integer :height
      t.text :note
      t.string :pic_url
      t.belongs_to :room_feature
      t.integer :rate_category_id
      t.integer :rate_id

      t.timestamps
    end
  end
end
