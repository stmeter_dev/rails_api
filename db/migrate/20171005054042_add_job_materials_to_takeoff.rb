class AddJobMaterialsToTakeoff < ActiveRecord::Migration[5.1]
  def self.up
  	add_column :takeoffs, :job_materials, :float
  end

  def self.down
  	remove_column :takeoffs, :job_materials
  end
end
