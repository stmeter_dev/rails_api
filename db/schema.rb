# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20172702184523) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "account_users", id: false, force: :cascade do |t|
    t.text "email"
    t.text "account"
  end

  create_table "accounts", id: :serial, force: :cascade do |t|
    t.integer "owner_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "addr_line1"
    t.string "addr_line2"
    t.string "zipcode"
    t.string "city"
    t.string "state"
    t.string "phone_number"
    t.string "cell_number"
    t.string "fax"
    t.string "logo"
    t.string "time_zone"
  end

  create_table "clients", id: :serial, force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "email"
    t.string "phone_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "full_name"
    t.integer "user_id"
  end

  create_table "estimates", id: :serial, force: :cascade do |t|
    t.datetime "start_time"
    t.datetime "end_time"
    t.text "work_description"
    t.integer "property_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "work_type"
    t.string "status"
    t.integer "user_id"
    t.index ["property_id"], name: "index_estimates_on_property_id"
  end

  create_table "exclusions", id: :serial, force: :cascade do |t|
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "proposal_id"
    t.index ["proposal_id"], name: "index_exclusions_on_proposal_id"
  end

  create_table "exterior_scopes", id: :serial, force: :cascade do |t|
    t.integer "proposal_id"
    t.decimal "materials"
    t.decimal "labor"
    t.decimal "total"
    t.index ["proposal_id"], name: "index_exterior_scopes_on_proposal_id"
  end

  create_table "feature_items", force: :cascade do |t|
    t.string "name"
    t.decimal "quantity"
    t.decimal "hours"
    t.decimal "coats"
    t.decimal "prep"
    t.decimal "material_quantity"
    t.integer "material_id"
    t.integer "width"
    t.integer "height"
    t.text "note"
    t.string "pic_url"
    t.bigint "room_feature_id"
    t.integer "rate_category_id"
    t.integer "rate_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["room_feature_id"], name: "index_feature_items_on_room_feature_id"
  end

  create_table "flex_rates", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.decimal "rate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "unit"
  end

  create_table "global_rates", force: :cascade do |t|
    t.decimal "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "category"
  end

  create_table "inclusions", id: :serial, force: :cascade do |t|
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "proposal_id"
    t.index ["proposal_id"], name: "index_inclusions_on_proposal_id"
  end

  create_table "int_features", id: :serial, force: :cascade do |t|
    t.string "code"
    t.integer "quantity"
    t.integer "property_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["property_id"], name: "index_int_features_on_property_id"
  end

  create_table "interior_scopes", id: :serial, force: :cascade do |t|
    t.integer "proposal_id"
    t.decimal "materials"
    t.decimal "labor"
    t.decimal "total"
    t.index ["proposal_id"], name: "index_interior_scopes_on_proposal_id"
  end

  create_table "materials", force: :cascade do |t|
    t.string "name"
    t.decimal "cov"
    t.decimal "cost"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "measures", force: :cascade do |t|
    t.string "name"
    t.string "category"
    t.integer "value"
    t.string "unit"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "production_rates", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "unit"
    t.integer "rate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "production_template_id"
    t.string "category"
  end

  create_table "production_templates", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "active", default: true, null: false
    t.string "category", default: "RES_INT", null: false
  end

  create_table "products", id: :serial, force: :cascade do |t|
    t.string "title", default: ""
    t.decimal "price", default: "0.0"
    t.boolean "published", default: false
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_products_on_user_id"
  end

  create_table "properties", id: :serial, force: :cascade do |t|
    t.string "addr_line1"
    t.string "addr_line2"
    t.string "zipcode"
    t.string "city"
    t.string "state"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "client_id"
    t.string "full_address"
    t.index ["client_id"], name: "index_properties_on_client_id"
  end

  create_table "proposals", id: :serial, force: :cascade do |t|
    t.integer "estimate_id"
    t.string "title"
    t.string "status"
    t.datetime "submit_date"
    t.decimal "amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "version"
    t.string "token"
    t.index ["estimate_id"], name: "index_proposals_on_estimate_id"
  end

  create_table "rate_categories", force: :cascade do |t|
    t.string "name"
    t.string "inout"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rates", force: :cascade do |t|
    t.bigint "rate_category_id"
    t.string "name"
    t.string "unit"
    t.integer "first_coat"
    t.integer "second_coat"
    t.integer "third_coat"
    t.integer "fourth_coat"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["rate_category_id"], name: "index_rates_on_rate_category_id"
  end

  create_table "room_features", id: :serial, force: :cascade do |t|
    t.string "name"
    t.integer "quantity"
    t.integer "room_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "hours"
    t.decimal "material_quantity"
    t.integer "material_id"
    t.decimal "labor_cost"
    t.decimal "material_cost"
    t.index ["room_id"], name: "index_room_features_on_room_id"
  end

  create_table "room_items", id: :serial, force: :cascade do |t|
    t.integer "room_scope_id"
    t.string "name"
    t.integer "quantity"
    t.integer "coats"
    t.decimal "cost"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["room_scope_id"], name: "index_room_items_on_room_scope_id"
  end

  create_table "room_scopes", id: :serial, force: :cascade do |t|
    t.integer "interior_scope_id"
    t.decimal "materials"
    t.decimal "labor"
    t.decimal "subtotal"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.index ["interior_scope_id"], name: "index_room_scopes_on_interior_scope_id"
  end

  create_table "rooms", id: :serial, force: :cascade do |t|
    t.string "name"
    t.integer "length"
    t.integer "width"
    t.integer "height"
    t.integer "property_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "number"
    t.boolean "painted_before", default: false
    t.integer "takeoff_id"
    t.decimal "hours"
    t.decimal "material_quantity"
    t.decimal "labor_cost"
    t.decimal "material_cost"
    t.integer "material_id"
    t.decimal "total_cost"
    t.index ["property_id"], name: "index_rooms_on_property_id"
  end

  create_table "side_features", id: :serial, force: :cascade do |t|
    t.string "code"
    t.integer "quantity"
    t.integer "side_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["side_id"], name: "index_side_features_on_side_id"
  end

  create_table "side_items", id: :serial, force: :cascade do |t|
    t.integer "side_scope_id"
    t.string "name"
    t.integer "quantity"
    t.integer "coats"
    t.decimal "cost"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["side_scope_id"], name: "index_side_items_on_side_scope_id"
  end

  create_table "side_scopes", id: :serial, force: :cascade do |t|
    t.integer "exterior_scope_id"
    t.decimal "materials"
    t.decimal "labor"
    t.decimal "subtotal"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "code"
    t.index ["exterior_scope_id"], name: "index_side_scopes_on_exterior_scope_id"
  end

  create_table "sides", id: :serial, force: :cascade do |t|
    t.string "code"
    t.integer "property_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "width", default: 0, null: false
    t.integer "height", default: 0, null: false
    t.integer "takeoff_id"
    t.index ["property_id"], name: "index_sides_on_property_id"
  end

  create_table "takeoffs", force: :cascade do |t|
    t.bigint "estimate_id"
    t.datetime "start"
    t.datetime "end"
    t.string "status"
    t.boolean "saved"
    t.decimal "work_hours"
    t.decimal "job_labor_cost"
    t.decimal "job_material_cost"
    t.decimal "job_total_cost"
    t.decimal "job_profit"
    t.decimal "job_tax"
    t.decimal "job_price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "sheet_url", default: ""
    t.float "job_materials"
    t.index ["estimate_id"], name: "index_takeoffs_on_estimate_id"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer "invitation_limit"
    t.string "invited_by_type"
    t.integer "invited_by_id"
    t.integer "invitations_count", default: 0
    t.string "auth_token", default: ""
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "time_zone"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.index ["auth_token"], name: "index_users_on_auth_token", unique: true
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["invitation_token"], name: "index_users_on_invitation_token", unique: true
    t.index ["invitations_count"], name: "index_users_on_invitations_count"
    t.index ["invited_by_id"], name: "index_users_on_invited_by_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
