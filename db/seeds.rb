# # This file should contain all the record creation needed to seed the database with its default values.
# # The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
# #
# # Examples:
# #
# #   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
# #   Character.create(name: 'Luke', movie: movies.first)

# if Material.count == 0
#   puts "Seeding materials table..."
#   Material.create(name: 'Benjamin Moore', cov: 1000 , cost: 3)
#   Material.create(name: 'BPP', cov: 1000 , cost: 5)
#   Material.create(name: 'Valspar American', cov: 1000 , cost: 4)
#   Material.create(name: 'Dunn-Edwards', cov: 1000 , cost: 2)
#   Material.create(name: 'Pratt & Lambert', cov: 1000 , cost: 8)
#   Material.create(name: 'Glidden', cov: 1000 , cost: 3)
#   Material.create(name: 'Sherwin-Williams', cov: 1000 , cost: 8)
# end

# if GlobalRate.count == 0
#   puts "Seeding global rates table..."
#   GlobalRate.create(name: 'profit', value: 10, 'category': 'percent')
#   GlobalRate.create(name: 'labor', value: 50, 'category': 'num')
#   GlobalRate.create(name: 'tax', value: 8, 'category': 'percent')
# end

# if FlexRate.count == 0
#   puts "Seeding flex rates table..."
#   FlexRate.create(name: 'Wall', rate: 1, unit: 'Hr/100 SQF')
#   FlexRate.create(name: 'Ceiling', rate: 1.5, unit: 'Hr/100 SQF')
#   FlexRate.create(name: 'Window', rate: 1, unit: 'ITEM')
#   FlexRate.create(name: 'Door', rate: 2, unit: 'ITEM')
#   FlexRate.create(name: 'Trim', rate: 1, unit: 'Hr/100 LF')
# end

# if RateCategory.count == 0
#   puts "Seeding rate categories table..."
#   RateCategory.create(name: 'Walls', unit: 'SQF/Hr', inout: 'IN')
#   RateCategory.create(name: 'Ceiling', unit: 'SQF/Hr', inout: 'IN')
#   RateCategory.create(name: 'Windows', unit: 'LF/Hr', inout: 'IN')
#   RateCategory.create(name: 'Doors', unit: 'SQF/Hr', inout: 'IN')
#   RateCategory.create(name: 'Base', unit: 'SQF/Hr', inout: 'IN')
#   RateCategory.create(name: 'Crown', unit: 'SQF/Hr', inout: 'IN')
#   RateCategory.create(name: 'Trim Work', unit: 'LF/Hr', inout: 'IN')
# end

# if Rate.count == 0
#   puts "Seeding rates table..."
#   Rate.create(name: 'Wall-F0-8', first_coat: 175, second_coat: 175, rate_category_id: 0)
#   Rate.create(name: 'Wall-F9-11', first_coat: 135, second_coat: 135, rate_category_id: 0)
#   Rate.create(name: 'Ceiling-0-8', first_coat: 185, second_coat: 185, rate_category_id: 1)
#   Rate.create(name: 'Base', first_coat: 40, second_coat: 40, rate_category_id: 4)
#   Rate.create(name: 'Crown', first_coat: 25, second_coat: 40, rate_category_id: 5)
#   Rate.create(name: 'Flush Door', first_coat: 75, second_coat: 75, rate_category_id: 3)
#   Rate.create(name: '1 Lite Windows', first_coat: 65, second_coat: 65, rate_category_id: 2)
# end

# if ProductionRate.count == 0
#   puts "Seeding production rates table..."
#   ProductionRate.create(name: 'Wall-F0-8', rate: 175, unit: 'Sf/hr', category: 'INT')
#   ProductionRate.create(name: 'Wall-F9-11', rate: 135, unit: 'Sf/hr', category: 'INT')
#   ProductionRate.create(name: 'Ceiling-0-8', rate: 185, unit: 'Sf/hr', category: 'INT')
#   ProductionRate.create(name: 'Base', rate: 40, unit: 'Sf/hr', category: 'INT')
#   ProductionRate.create(name: 'Crown', rate: 25, unit: 'Sf/hr', category: 'INT')
#   ProductionRate.create(name: 'Door', rate: 75, unit: 'Sf/hr', category: 'INT')
#   ProductionRate.create(name: 'Window', rate: 65, unit: 'Ft/hr', category: 'INT')
# end

# if Measure.count == 0
#   puts "Seeding measures table..."
#   Measure.create(name: 'Flush', value: 40, unit: 'SF', category: 'Doors')
#   Measure.create(name: '1 Lite', value: 55, unit: 'SF', category: 'Doors')
#   Measure.create(name: '1 + Panels', value: 70, unit: 'SF', category: 'Doors')
#   Measure.create(name: '10 Lite 7`', value: 70, unit: 'SF', category: 'Doors')
#   Measure.create(name: '10 Lite 8`', value: 85, unit: 'SF', category: 'Doors')
#   Measure.create(name: '1 Lite Slider Unit', value: 150, unit: 'SF', category: 'Doors')
#   Measure.create(name: '1 Lite New Slider', value: 150, unit: 'SF', category: 'Doors')
#   Measure.create(name: 'Frame only', value: 30, unit: 'SF', category: 'Doors')

#   Measure.create(name: 'Window Sills', value: 5, unit: 'LF', category: 'Windows')
#   Measure.create(name: 'Small Wnd Frame', value: 25, unit: 'LF', category: 'Windows')
#   Measure.create(name: '1 Lite Windows: Small', value: 35, unit: 'LF', category: 'Windows')
#   Measure.create(name: '1 Lite Windows: Large', value: 45, unit: 'LF', category: 'Windows')
#   Measure.create(name: 'French 2-7 Lite', value: 55, unit: 'LF', category: 'Windows')
#   Measure.create(name: 'French 8-12 Lite', value: 70, unit: 'LF', category: 'Windows')
#   Measure.create(name: 'French 12-16 Lite', value: 85, unit: 'LF', category: 'Windows')
#   Measure.create(name: 'Large French 16+ Lites', value: 100, unit: 'LF', category: 'Windows')

#   Measure.create(name: 'Basic Fireplace', value: 50, unit: 'LF', category: 'Trim Work')
#   Measure.create(name: 'Basic Mantel', value: 15, unit: 'LF', category: 'Trim Work')
#   Measure.create(name: 'Detailed Fireplace', value: 100, unit: 'LF', category: 'Trim Work')
#   Measure.create(name: 'Plantation Shutters', value: 6, unit: 'LF', category: 'Trim Work')

# end
